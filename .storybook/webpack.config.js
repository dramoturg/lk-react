process.argv.push('--scripts-version');
process.argv.push('react-scripts');

const craConfig =
	process.env.NODE_ENV.toLowerCase() === 'production'
		? require('react-scripts/config/webpack.config.prod')
		: require('react-scripts/config/webpack.config.dev');

const overrideConfig = require('../config-overrides');

const patchDefaultLoader = config => {
	const rules = config.module.rules[config.module.rules.length - 1].oneOf;
	const defaultLoader = rules[rules.length - 1];
	defaultLoader.exclude.push(/\.ejs$/);
};

module.exports = (storybookConfig, env) => {
	const entry = storybookConfig.entry;
	const output = storybookConfig.output;

	const storybookHTMLPlugins = storybookConfig.plugins.filter(
		plugin => plugin.constructor.name === 'HtmlWebpackPlugin',
	);

	const craPluginsWithoutHTML = craConfig.plugins.filter(plugin => plugin.constructor.name !== 'HtmlWebpackPlugin');
	patchDefaultLoader(craConfig);

	return overrideConfig(
		{
			...craConfig,
			entry,
			output,
			plugins: [...craPluginsWithoutHTML, ...storybookHTMLPlugins],
		},
		env,
	);
};
