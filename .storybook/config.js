import React from 'react';
import { configure, addDecorator } from '@storybook/react';
import { setOptions } from '@storybook/addon-options';
import { withKnobs } from '@storybook/addon-knobs';

const req = require.context('../src', true, /\.story.jsx/);
function loadStories() {
	req.keys().forEach(filename => req(filename));
}

// Option defaults:
setOptions({
	name: 'Small Exchange Storybook',
	showStoriesPanel: true,
	showAddonPanel: true,
	addonPanelInRight: false,
	sidebarAnimations: true,
	selectedAddonPanel: 'storybooks/storybook-addon-knobs',
});

addDecorator(story => {
	return React.createElement(
		React.Fragment,
		null,
		React.createElement('style', null, 'body, html, #root {height: 100%; background: #f6f6f6}'),
		story(),
	);
});
addDecorator(withKnobs);

configure(loadStories, module);
