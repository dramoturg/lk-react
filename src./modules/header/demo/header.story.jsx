import React from "react";
import {storiesOf} from "@storybook/react";
import {Theme} from "../../ui-kit/components/theme/theme.component";
import {Header} from "../header";


storiesOf("Header", module)
	.add("primary", () => (
		<Theme>
			<Header/>
		</Theme>
	))
