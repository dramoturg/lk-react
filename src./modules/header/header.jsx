import React from 'react';
import {BalanceInfo} from "./components/balance-info/balance-info.component";
import {AccountInfo} from './components/accounf-info/account-info.component';
import {Input} from "../ui-kit/components/input/input.component";
import {Link} from "@devexperts/react-kit/dist/components/Link/Link";
import {ButtonIcon} from "../ui-kit/components/button-icon/button-icon.component";
import {BurgerIcon} from "../ui-kit/components/icons/burger.icon";

import refLinkIcon from './icons/ref-link.png';
import logo from './images/logo.png';

import css from './theme/header.component.module.styl';
import buttonIconTheme from './theme/button-icon.child.module.styl';
import linkTheme from './theme/link.child.module.styl';
import inputTheme from './theme/input.child.module.styl';

export class Header extends React.Component {
	render() {
		return (
			<div className={`${css.container}`}>
				<ButtonIcon theme={buttonIconTheme} icon={<BurgerIcon/>}/>
				<div className={css.logo}>
					<img src={logo} alt=""/>
				</div>
				{/*<div className={`${css.suggest} d-md-none d-lg-block`}>
					<Input theme={inputTheme}/>
				</div>
				<div className={css.accountInfo}>
					<AccountInfo/>
				</div>
				<div className={`${css.balanceInfo} d-md-none d-lg-block`}>
					<BalanceInfo/>
				</div>

				<div className={`${css.refLink} d-md-none d-lg-block`}>
					<div className={css.refLinkLabel}> Моя реферальная ссылка</div>
					<Link href='#' theme={linkTheme}>
						<img src={refLinkIcon}/>
						собиратьлегко.рф/acc/ivanov
					</Link>
				</div>*/}

				{/*<Button isFlat={true}>
					<img src={logoutIcon} style={{
						width: '24px',
						height: '24px'
					}} />
				</Button>*/}
			</div>
		)
	}
}
