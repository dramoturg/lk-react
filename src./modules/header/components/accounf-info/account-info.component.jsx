import React from 'react';
import moveToNextIcon from './icon/move-to-next.png';
import {Button} from "../../../ui-kit/components/button/button.component";
import {Popover} from "../../../ui-kit/components/popover/popover.component";
import {AccountIcon} from "../../../ui-kit/components/icons/account.icon";

import css from './theme/account-info.component.module.styl';
import buttonTheme from './theme/button.child.module.styl';

export class AccountInfo extends React.Component {
	anchor;

	state = {
		isOpened: false,
	};

	render() {
		const { isOpened } = this.state;

		return (
			<Button isFlat={true} ref={el => (this.anchor = el)} onClick={this.handleClick} theme={buttonTheme}>
				<div className={`${css.accountIcon} d-none d-sm-block`}>
					<AccountIcon />
				</div>
				<div className={`${css.container} d-md-none d-lg-block`}>
					<div className={css.anchorInner}>
						Иванов Евгений Александрович
						<div className={css.toggleIcon}>
							<img src={moveToNextIcon}/>
						</div>
					</div>
				</div>
				<Popover
					isOpened={isOpened}
					closeOnClickAway={true}
					onRequestClose={this.handleRequestClose}
					hasArrow={false}
					anchor={this.anchor}>
					<div style={{
						padding: '10px 100px'
					}}>Account info</div>
				</Popover>
			</Button>
		)
	}

	handleClick = () => {
		this.setState({
			isOpened: !this.state.isOpened,
		});
	};

	handleRequestClose = () => {
		console.log('rt')
		this.setState({
			isOpened: false,
		});
	};
}
