import React from 'react';
import css from './theme/balance-info.component.module.styl';
import rubleIcon from './icon/ruble.png';
import moveToNextIcon from './icon/move-to-next.png';
import {Button} from "../../../ui-kit/components/button/button.component";
import {Popover, PopoverAlign} from "../../../ui-kit/components/popover/popover.component";
console.log(PopoverAlign.Top);
export class BalanceInfo extends React.Component {
	anchor;

	state = {
		isOpened: false,
	};

	render() {
		const { isOpened } = this.state;

		return (
			<Button isFlat={true} ref={el => (this.anchor = el)} onClick={this.handleClick}>
				<div className={css.container}>
					<div className={css.icon}>
						<img src={rubleIcon}/>
					</div>
					<div className={css.anchorInner}>
						Баланс
						<div className={css.toggleIcon}>
							<img src={moveToNextIcon}/>
						</div>
					</div>
				</div>
				<Popover
					isOpened={isOpened}
					closeOnClickAway={true}
					onRequestClose={this.handleRequestClose}
					hasArrow={false}
					anchor={this.anchor}>

					<ul className={css.metrics}>
						<li>Баланс: 990 руб</li>
						<li>Выписка ЕГРН: 890 шт.</li>
						<li>
							Контроль изменений ЕГРН<br/>
							оплачен до 30 сент. 2018
						</li>
						<li>Собраний: 3 шт.</li>
						<li>Консультаций: 1 шт.</li>
					</ul>

				</Popover>
			</Button>
		)
	}

	handleClick = () => {
		this.setState({
			isOpened: !this.state.isOpened,
		});
	};

	handleRequestClose = () => {
		this.setState({
			isOpened: false,
		});
	};
}
