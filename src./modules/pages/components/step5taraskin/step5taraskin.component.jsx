import React from 'react';
import {DownBlock} from "../../../ui-kit/components/down-block/down-block.component";
import {Page} from "../../../ui-kit/components/page/page.component";
import { Table, TableCell as Cell, TableHead as THead, TableBody as TBody, TableRow as Tr } from "../../../ui-kit/components/table/table.component";
import {Checkbox} from "../../../ui-kit/components/dx-checkbox/checkbox.component";
import {PinCell} from "../../../ui-kit/components/cells/pin-cell.component";
import {DateCell} from "../../../ui-kit/components/cells/date-cell.component";
import {Col, Container, Row} from "reactstrap";

export class Step5Taraskin extends React.Component {
	render() {
		return (
			<Page title="Карточка дома/выбор помещений">
				<Container fluid>
					<Row>
						<Col xs={12}>
							<Table>
								<THead>
								<Tr>
									<Cell><Checkbox /></Cell>
									<Cell>Кадастровый номер</Cell>
									<Cell>Адрес</Cell>
									<Cell>Площадь</Cell>
									<Cell>Тип помещения</Cell>
									<Cell>Этаж</Cell>
									<Cell>Дата</Cell>
									<Cell>Собственников</Cell>
									<Cell>Комментарий</Cell>
								</Tr>
								</THead>
								<TBody>
								<Tr>
									<Cell><Checkbox /></Cell>
									<Cell>78:14:0007685:4042</Cell>
									<PinCell>
										г. Санкт-Петербург, Московское ш., д. 30, <br/> корп. 2, лит. А, кв. 1
									</PinCell>
									<Cell>52,2</Cell>
									<Cell>Нежилое помещение</Cell>
									<Cell>Чердак</Cell>
									<DateCell>10.05.2018</DateCell>
									<Cell>2</Cell>
									<Cell>Скорее всего нет данных</Cell>
								</Tr>
								<Tr>
									<Cell><Checkbox /></Cell>
									<Cell>78:14:0007685:4042</Cell>
									<PinCell>г. Санкт-Петербург, Московское ш., д. 30, <br/> корп. 2, лит. А, кв. 1</PinCell>
									<Cell>52,2</Cell>
									<Cell>Нежилое помещение</Cell>
									<Cell>Чердак</Cell>
									<DateCell>10.05.2018</DateCell>
									<Cell>2</Cell>
									<Cell>Скорее всего нет данных</Cell>
								</Tr>
								<Tr>
									<Cell><Checkbox /></Cell>
									<Cell>78:14:0007685:4042</Cell>
									<PinCell>г. Санкт-Петербург, Московское ш., д. 30, <br/> корп. 2, лит. А, кв. 1</PinCell>
									<Cell>52,2</Cell>
									<Cell>Нежилое помещение</Cell>
									<Cell>Чердак</Cell>
									<DateCell>10.05.2018</DateCell>
									<Cell>2</Cell>
									<Cell>Скорее всего нет данных</Cell>
								</Tr>
								<Tr>
									<Cell><Checkbox /></Cell>
									<Cell>78:14:0007685:4042</Cell>
									<PinCell>г. Санкт-Петербург, Московское ш., д. 30, <br/> корп. 2, лит. А, кв. 1</PinCell>
									<Cell>52,2</Cell>
									<Cell>Нежилое помещение</Cell>
									<Cell>Чердак</Cell>
									<DateCell>10.05.2018</DateCell>
									<Cell>2</Cell>
									<Cell>Скорее всего нет данных</Cell>
								</Tr>
								</TBody>
							</Table>
						</Col>
					</Row>
					<DownBlock />
				</Container>
			</Page>
		);
	}
}
