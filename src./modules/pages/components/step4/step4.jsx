import React from 'react';
import {Page} from "../../../ui-kit/components/page/page.component";
import {Tariff} from "../../../cards/component/tariff-card/tariff-card.component";
import {CustomForm} from "../../../custom-form/custom-form.component";
import {Col, Container, Row, Breadcrumb, BreadcrumbItem} from 'reactstrap';
import {PaymentMethod, AvailableType} from "../../../payment-method/payment-method.component";
import {Section} from "../../../ui-kit/components/section/section.component";
import {Hr} from "../../../ui-kit/components/hr/hr.component";
import {PaymentForm} from "../../../forms/components/payment-form/payment-form.component";
import iconArrow from '../../../ui-kit/components/page/icons/arrow.png';
import {PrepaidStatement} from "../../../cards/component/prepaid-statement/prepaid-statements.component";
import {Button} from "../../../ui-kit/components/button/button.component";
import {WellDonePopup} from "../../../popups/components/well-done-popup/well-done.popup.component";

export class Step4 extends React.Component {
	state = {
		isOpened: true
	}
	render() {
		const {isOpened} = this.state;
		return (
			<Page withSteps={true} step={2} title="Мастер подготовки реестра помещений МКД 3 этап">
				<WellDonePopup isOpened={isOpened} onRequestClose={this.onRequestClose} />
				<Container fluid>
					<Row>
						<Col xs={12}>
							<PrepaidStatement countAvailable={1000} />
							<Section isMain={true} header="Тарифы">
								<p>
									Оплатить можно как поштучно за помещения, так и приобрести тариф
									с более выгодной стоимостью помещения. <br/>
									Большинство наших клиентов поступают именно так!
								</p>
							</Section>
						</Col>
					</Row>
					<Row>
						<Col xs={12} className="d-flex flex-wrap">
							<Tariff title="100 помещений за 3000 руб" discount={10}>
								<ul>
									<li>Выписки по 100 помещения</li>
									<li>Собрания 1 шт</li>
									<li>Консультации 0 шт</li>
								</ul>
							</Tariff>
							<Tariff title="100 помещений за 20000 руб" discount={20}>
								<ul>
									<li>Выписки по 100 помещения</li>
									<li>Собрания 1 шт</li>
									<li>Консультации 0 шт</li>
								</ul>
							</Tariff>
							<Tariff title="100 помещений за 20000 руб" discount={30}>
								<ul>
									<li>Выписки по 100 помещения</li>
									<li>Собрания 1 шт</li>
									<li>Консультации 0 шт</li>
								</ul>
							</Tariff>
							<Tariff title="Сборный тариф" isCustom={true}>
								<CustomForm />
							</Tariff>
						</Col>
					</Row>
					<Row>
						<Col xs={12}>
							<Hr/>
							<Section isMain={true} header="Оплата">
								<PaymentForm />
							</Section>
						</Col>
					</Row>
					<Row>
						<Col xs={12}>
							<Section isMain={true} header={<h2>Способ оплаты</h2>}>
								<div className="d-flex">
									<PaymentMethod title="Перевод на банковскую карту СберБанк «Онлайн»"
									               type={AvailableType.Sberbank}
									               isChecked={true}/>
									<PaymentMethod title="Банковая карта (Yandex)"
									               type={AvailableType.Yanex}>
										Visa, MasterCard или Мир
									</PaymentMethod>
									<PaymentMethod title="Перевод на расчетный счет"
									               type={AvailableType.Transfer}>
										Безналичный платёж через банк
									</PaymentMethod>
								</div>
							</Section>

						</Col>
					</Row>
					<Row>
						<Col xs={12}>
							<p>Как оплатить?</p>

							<Breadcrumb>
								<BreadcrumbItem>
									<a href="#">Пластиковой картой</a>
									<img src={iconArrow} />
								</BreadcrumbItem>
								<BreadcrumbItem>
									<a href="#">Банковским переводом</a>
									<img src={iconArrow} />
								</BreadcrumbItem>
								<BreadcrumbItem>
									<a href="#">Электронные деньги</a>
									<img src={iconArrow} />
								</BreadcrumbItem>
							</Breadcrumb>
						</Col>
					</Row>
					<Row>
						<Col xs={12}>
							<Button isPrimary={true}>Загрузить</Button>
						</Col>
					</Row>
				</Container>
			</Page>
		);
	}

	onRequestClose = () => {
		this.setState({
			isOpened: false
		})
	}
}
