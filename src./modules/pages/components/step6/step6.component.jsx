import React from 'react';
import { memoize } from '@devexperts/utils/dist/function/memoize';
import {Page} from "../../../ui-kit/components/page/page.component";
import {Tabs} from "../../../ui-kit/components/tabs/tabs.component";
import {Tab} from "../../../ui-kit/components/tabs/tab.component";
import {Col, Container, Row} from "reactstrap";
import {Section} from "../../../ui-kit/components/section/section.component";
import {BuildingInfo} from "../../../ui-kit/components/bulding-info/building-info.component";
import { YMaps, Map, Placemark } from 'react-yandex-maps';
import {ServiceCard} from "../../../cards/component/service-card/service-card.component";
import {Card} from "../../../ui-kit/components/card/card.component";
import {SummaryCard} from "../../../cards/component/summary-card/summary-card.component";

const metricks = [
	{
		title: 'Кадастровый номер',
		value: '78:34:0415601:11329'
	}, {
		title: 'Кол-во помещ.',
		value: 390
	}, {
		title: 'Кол-во жилых',
		value: 370
	}, {
		title: 'Кол-во нежилых',
		value: 20
	}, {
		title: 'S жилых помещ.',
		value: '370 м2'
	}, {
		title: 'S нежилых помещ.',
		value: '30 м2'
	}, {
		title: 'S общая',
		value: '21 000 м2'
	}
]

export class Step6 extends React.Component {
	render() {
		const activeTab = 1;
		const mapState = { center: [55.76, 37.64], zoom: 10, controls: [] };

		return (
			<Page step={3} withSteps={true} title='Карточка дома/выбор помещений'>
				<Tabs>
					<Tab isActive={activeTab === 1} handleClick={this.handleTabClick(1)}>
						Общая информация
					</Tab>
					<Tab isActive={activeTab === 2} handleClick={this.handleTabClick(2)}>
						Реестры
					</Tab>
					<Tab isActive={activeTab === 3} handleClick={this.handleTabClick(3)}>
						собрания
					</Tab>
				</Tabs>

				<Container fluid={true}>
					<Row>
						<Col md={7}>
							<Section>


								<BuildingInfo metricks={metricks} />

							</Section>

							<Section header={<div>Образцы документов</div>}>
								<ul>
									<li>Скачать Уведомление о проведении собрания собственников жилья</li>
									<li>Скачать Бюллетень голосования (решение) собственников жилья на общем собрании собственников МКД</li>
									<li>Скачать Протокол собрания собственников МКД</li>
									<li>Скачать реестр участников очного собрания</li>
									<li>Уведомления администрации (сопроводительные письма)</li>
									<li>Уведомления в управляющую компанию (сопроводительные письма)</li>
									<li>Доверенность на проведение собрания собственников</li>
									<li>Договор на проведение собрания собственников жилья</li>
									<li>Акты о размещении сообщений о проведении внеочередного общего собрания собственников помещений в форме очно-заочного голосования в МКД</li>
								</ul>
							</Section>

							<Section header={<div>Документация</div>}>
								<ul>
									<li>Скачать Ст. ЖК РФ № 44 № 45 № 46 № 47 № 48 Обновление 12.06.2018 г.</li>
									<li>Скачать Приказ № 937/пр&nbsp;Об утверждении Требований к оформлению протоколов и Порядка передачи решений и протоколов общих собраний собственников помещений в МКД в органы исполнительной власти с обновлениями от 09 апреля 2018 г.</li>
									<li>Скачать Приказ № 938/пр&nbsp;Об утверждении Порядка и сроков внесения изменений в реестр лицензий субъекта РФ с обновлениями от 09 апреля 2018 г. ЖК РФ</li>
								</ul>
							</Section>
						</Col>
						<Col md={5}>

							<SummaryCard unloaded={370} total={390} >

								<ServiceCard type={ServiceCard.Consult}>
									Получить консультацию по процедуре
									проведения собрания
								</ServiceCard>

								<ServiceCard type={ServiceCard.Zakaz}>
									Заказать собраниея собствеников жилья МКД
									<small>
										только в санкт-петербурге и
										ленинградской области
									</small>
								</ServiceCard>

								<ServiceCard type={ServiceCard.Self}>
									Провести собрание самостоятельно
								</ServiceCard>

							</SummaryCard>




							<Section>
								<h2>Местоположение на карте</h2>
								<YMaps>
									<Map state={mapState} width="100%" height="215px" options={{suppressMapOpenBlock: true}}>
										<Placemark
											geometry={{
												coordinates: [55.751574, 37.573856]
											}}
											properties={{
												hintContent: 'Собственный значок метки',
												balloonContent: 'Это красивая метка'
											}}
											options={{}}
										/>

									</Map>
								</YMaps>
							</Section>
						</Col>
					</Row>
				</Container>
			</Page>
		);
	}

	handleTabClick = memoize((index) => () => {
		this.setState({
			activeTab: index,
		});
	});
}
