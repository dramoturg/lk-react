import React from 'react';
import appointmentIcon from './icons/appointment.png';
import eventsIcon from './icons/events.png';
import houseIcon from './icons/house.png';
import inboxIcon from './icons/inbox.png';
import cn from 'classnames';
import {Collapse} from "reactstrap";

import {Button} from "../../../ui-kit/components/button/button.component";
import {MoveToNext2} from "../../../ui-kit/components/icons/move-to-next2.icon";

import css from './theme/sidebar.component.module.styl';

const togglerButtonTheme = {
	container: css.togglerButton
}

export class Sidebar extends React.Component {

	state = {
		expandedKeys: []
	}

	render() {
		const {isCollapsed, onToggle} = this.props;
		const {expandedKeys} = this.state;
		const className = cn(css.container, {
			[css.container_collapsed]: isCollapsed,
		});

		const isExpand = key => expandedKeys.includes(key);

		const classNameActiveMenuItem = cn(css.menuItem, {
			[css.menuItem_active]: true
		});

		return (
			<div className={`${className} d-none d-lg-block`}>
				<div className={css.toggler} onClick={onToggle}>
					<div className={css.togglerIcon}>
						<MoveToNext2 />
					</div>
					<Button isFlat={true} theme={togglerButtonTheme}>
						СВЕРНУТЬ
					</Button>
				</div>
				<nav>
					<ul>
						<li className={css.menuItem}>
							<div className={css.menuGroup}>
								<div className={css.icon}>
									<img src={appointmentIcon} />
								</div>
								<div className={css.link}>
									<a href="#">Календарь событий</a>
								</div>
							</div>
						</li>
						<li className={css.menuItem}>
							<div className={css.menuGroup}>
								<div className={css.icon}>
									<img src={houseIcon} />
								</div>
								<div className={css.link}>
									<a href="#">Все дома</a>
								</div>
							</div>
						</li>
						<li className={classNameActiveMenuItem}>
							<div className={css.menuGroup} onClick={this.onClick(1)}>
								<div className={css.icon}>
									<img src={inboxIcon} />
								</div>
								<div className={css.link}>
									<a href="#">Все реестры</a>
								</div>
								<div className={css.togglerIcon}>
									<MoveToNext2 />
								</div>
							</div>
							<Collapse isOpen={isExpand(1)}>
								<ul className={css.subMenu}>
									<li className={css.menuItem}>
										<div className={css.link}>
											<a href="#">
												Мастер одиночной <br />
												подготовки реестров
											</a>
										</div>
									</li>
									<li className={css.menuItem}>
										<div className={css.link}>
											<a href="#">
												Мастер массовой <br />
												подготовки реестров
											</a>
										</div>
									</li>
									<li className={css.menuItem}>
										<div className={css.link}>
											<a href="#">
												Незавершенные заказы
											</a>
											<div className={css.badge}>5</div>
										</div>
									</li>
								</ul>
							</Collapse>
						</li>
						<li className={css.menuItem}>
							<div className={css.menuGroup} onClick={this.onClick(2)}>
								<div className={css.icon}>
									<img src={eventsIcon} />
								</div>
								<div className={css.link}>
									<a href="#">Все события</a>
								</div>
								<div className={css.togglerIcon}>
									<MoveToNext2 />
								</div>
							</div>
							<Collapse isOpen={isExpand(2)}>
								<ul className={css.subMenu}>
									<li className={css.menuItem}>
										<div className={css.link}>
											<a href="#">Все проекты</a>
										</div>
									</li>
									<li className={css.menuItem}>
										<div className={css.link}>
											<a href="#">
												Мастер одиночного <br/>
												проведения собрания
											</a>
										</div>
									</li>
									<li className={css.menuItem}>
										<div className={css.link}>
											<a href="#">
												Мастер массового <br/>
												проведения собраний
											</a>
										</div>
									</li>
								</ul>
							</Collapse>
						</li>
					</ul>
				</nav>
			</div>
		)
	}

	onClick = key => () => {
		const {expandedKeys} = this.state;
		let newValue;
		if (expandedKeys.includes(key)) {
			newValue = expandedKeys.filter(item => item !== key);
		} else {
			newValue = [
				...expandedKeys,
				key
			]
		}
		this.setState({
			expandedKeys: newValue
		})
	}
}
