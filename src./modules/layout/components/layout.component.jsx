import React from 'react';
import {Sidebar} from "./sidebar/sidebar.component";
import {Footer} from "./footer/footer.component";
import {Header} from "../../header/header";
import { withTheme } from '@devexperts/react-kit/dist/utils/withTheme';
import * as css from './theme/layout.component.module.styl';

class RawLayout extends React.Component {

	state = {
		isCollapsed: true
	}

	render() {
		const {isCollapsed} = this.state;
		const {children} = this.props;

		return (
			<div className={css.container}>
				<Header/>
				<div className={css.wrapper}>
					<Sidebar isCollapsed={isCollapsed} onToggle={this.onToggle}/>
					<div className={css.content}>
						{children}
					</div>
				</div>
				<Footer />
			</div>
		)
	}

	onToggle = () => {
		this.setState({
			isCollapsed: !this.state.isCollapsed
		})
	}
}

const theme = {
	...css,
};

export const Layout = withTheme(Symbol(), theme)(RawLayout);
