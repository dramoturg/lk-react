import React from 'react';
import { Component } from 'react';
import { withTheme } from '@devexperts/react-kit/dist/utils/withTheme';
import css from './theme/form-control.component.module.styl';

class RawFormControl extends Component {
	render() {
		const { theme, title, children } = this.props;

		return (
			<div className={theme.container}>
				{title && (
					<div className={theme.title}>
						{title}
					</div>
				)}
				{children}
			</div>
		)
	}
}

const theme = {
	...css,
};

export const Badge = withTheme(Symbol(), theme)(RawFormControl);
