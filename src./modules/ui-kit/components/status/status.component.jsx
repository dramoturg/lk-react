import React, {Component} from 'react';
import cn from 'classnames';
import css from './theme/status.component.module.styl';

export const StatusType = {
	Pending: 'pending', // ожидает оплаты
	Prepare: 'prepare', // реестр готовится
	Ready: 'ready', // реестр готов можно скачать

	Download: 'download',

	Pay: 'pay'
}

export class Status extends Component {
	render() {
		const { children, type } = this.props;
		console.log(type, children);
		const className = cn(css.container, {
			[css.container_pandingPayment]: type === StatusType.Pending,
			[css.container_prepare]: type === 'prepare',
			[css.container_pay]: type === 'pay',
			[css.container_ready]: type === StatusType.Ready
		});

		return (
			<div className={className}>
				{type  === StatusType.Pay && 'Оплатить'}
				{children}
			</div>
		)
	}
}
