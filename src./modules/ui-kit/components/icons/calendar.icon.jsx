import * as React from 'react';
import {IconWrapper} from "../icon-wrapper/icon-wrapper.component";

export const CalendarIcon = () => (
	<IconWrapper>
		<svg viewBox="0 0 19 20">
			<image id="calendar-icon" width="19" height="20"
				   xlinkHref="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAUCAMAAABYi/ZGAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAz1BMVEWZmJj///+ZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJgAAADGIE7kAAAAQ3RSTlMAAAGAYgqSRRzrS7wbkLAg8CGPUMA3q2wDyhX+4TDQQLNqYEISTlIenIECiVkGT5MR3M0svzuvSCokiENJ/M6jpv0vvXrRmAAAAAFiS0dERPm0mMEAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfiCgESNxNuT/kZAAAArElEQVQY062P1w6CQBREL0XBtio2FNeCYu8VFev8/z+5YjRZE+OL5+nmJJOZS0SkajqFRKIGKYryOE3Eni6OROiSKZZGJssEVg75QrFkUxmoOFUOQa2ORg5oEuBSi7ddgad2ut0eisKRhAv2zal9fTAcja1JZGq83MyZm1gssVpj8862OG198nai4y9ZTR/sR/KWZ/Zj84FT4P/44wgmccKZLvjgGpDtuTI35Q5TISoclplGggAAAABJRU5ErkJggg=="/>
		</svg>
	</IconWrapper>
);
