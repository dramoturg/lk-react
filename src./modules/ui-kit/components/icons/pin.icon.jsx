import * as React from 'react';
import {IconWrapper} from '@devexperts/react-kit/dist/components/icon-wrapper/icon-wrapper.component';

export const PinIcon = () => (
	<IconWrapper>
		<svg
			width="0.167in" height="0.222in">
			<image  x="0px" y="0px" width="12px" height="16px"  xlinkHref="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAQCAMAAAAVv241AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAn1BMVEX///+ZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJj///8QyE6DAAAAM3RSTlMACXXN9/bJbgcj2NEcDNkGem3V83Z7xvt5hu9/j/HbifqOgTL+JbCiJPAeal2UuwMKtatBkEO0AAAAAWJLR0QAiAUdSAAAAAd0SU1FB+IKCxcvBXH29p4AAAB9SURBVAjXRY7XDsIwEAQH4tACmF4DoSb0dv//b5zjEOZhZ1eWrAMq1cCEtXoDpdmSnKgNna4URJaelPQZaA5HY6OaMNWcwVy1wL0vIVatWGuaJN6oArb/D3bsy344QvobmV5wCn0/X9w9Vz9u5GSu333HPkTSZzF4vT/W+QsMqhhYYp9e3QAAAABJRU5ErkJggg==" />
		</svg>
	</IconWrapper>
);
