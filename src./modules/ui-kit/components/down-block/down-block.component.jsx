import React from 'react';
import {Button} from "../button/button.component";

import css from './theme/down-block.component.module.styl';
import buttonTheme from './theme/button.child.module.styl';

export class DownBlock extends React.Component {
	render() {
		return (
			<div className={css.container}>
				<div className={css.selected}>
					<span className={css.label}>ВЫБРАНО</span>:
					<span className={css.count}> 390 </span> из них
					<span className={css.count}> 370 </span>жилых помещений, <span className={css.count}>20</span> нежилых помещений
				</div>
				<div className={css.buttons}>
					<Button theme={buttonTheme}>
						Сохранить и вернуться
					</Button>
					<Button theme={buttonTheme}>
						Сохранить выборку и оформить заказ
					</Button>
				</div>
			</div>
		)
	}
}
