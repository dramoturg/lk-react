import * as React from 'react';
import { withTheme } from '@devexperts/react-kit/dist/utils/withTheme';
import css from './theme/tabs.component.module.styl';

const RawTabs = props => {
	const { theme, children } = props;

	return <nav className={theme.tabs}>{children}</nav>;
};

export const Tabs = withTheme(Symbol(), css)(RawTabs);
