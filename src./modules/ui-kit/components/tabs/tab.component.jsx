import React from 'react';
import { Component } from 'react';
import * as cn from 'classnames';
import { withTheme } from '@devexperts/react-kit/dist/utils/withTheme';
import css from './theme/tab.component.module.styl';

class RawTab extends Component {
	render() {
		const { theme, isActive, handleClick, children } = this.props;

		const tabTheme = cn(theme.container, theme.tab, {
			[theme.tab_active]: isActive,
		});

		return (
			<div className={tabTheme} onClick={handleClick}>
				{children}
			</div>
		);
	}
}

export const Tab = withTheme(Symbol(), css)(RawTab);
