import { LINK } from '@devexperts/react-kit/dist/components/Link/Link';
import linkCss from '../link/theme/link.component.module.styl';

import { SELECTBOX } from '@devexperts/react-kit/dist/components/Selectbox/Selectbox';
import selectboxCss from '../selectbox/theme/selectbox.component.module.styl';

import {POPUP} from "@devexperts/react-kit/dist/components/Popup/Popup";
import popupCss from '../popup/popup.component.module.styl';

import { BUTTON } from '@devexperts/react-kit/dist/components/Button/Button';
import buttonCss from '../button/theme/button.component.module.styl';

import { BUTTON_ICON } from '@devexperts/react-kit/dist/components/ButtonIcon/ButtonIcon';
import buttonIconCss from '../button-icon/theme/button-icon.component.module.styl';

import {CHECKBOX} from "@devexperts/react-kit/dist/components/Checkbox/Checkbox";
import checkboxCss from '../dx-checkbox/checkbox.component.module.styl';

import { MENU } from '@devexperts/react-kit/dist/components/Menu/Menu';
import menuCss from '../menu/theme/menu.component.module.styl';

import { LIST } from '@devexperts/react-kit/dist/components/List/List';
import listCss from '../list/theme/list.component.module.styl';

import { POPOVER } from '@devexperts/react-kit/dist/components/Popover/Popover';
import popoverCss from '../popover/theme/popover.component.module.styl';

import { INPUT } from '@devexperts/react-kit/dist/components/input/Input';
import inputCss from '../input/theme/input.component.module.styl';

import { TABLE } from '@devexperts/react-kit/dist/components/Table/Table';
import tableCss from '../table/theme/table.component.module.styl';

export const CONTEXT_THEME = {
	[POPUP]: popupCss,
	[TABLE]: tableCss,
	[INPUT]: inputCss,
	[POPOVER]: popoverCss,
	[LINK]: linkCss,
	[LIST]: listCss,
	[MENU]: menuCss,
	[SELECTBOX]: selectboxCss,
	[BUTTON]: buttonCss,
	[BUTTON_ICON]: buttonIconCss,
	[CHECKBOX]: checkboxCss
};
