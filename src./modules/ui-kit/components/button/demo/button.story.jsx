import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { Theme } from '../../theme/theme.component'
import { Button } from "../button.component";


storiesOf("Button", module)
	.add("primary", () => (
		<Theme>
			<Button isPrimary={true} onClick={action("clicked")}>
				Загрузить
			</Button>
		</Theme>
	))