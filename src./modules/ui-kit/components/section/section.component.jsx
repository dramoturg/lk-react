import React from 'react';
import cn from 'classnames';
import css from './theme/section.component.module.styl';

export class Section extends React.Component {
	render() {
		const {children, isMain, header} = this.props;
		const className = cn(css.container, {
			[css.container_isMain]: isMain
		})
		return (
			<div className={className}>
				{header && (
					<div className={css.header}>
						{header}
					</div>
				)}
				<div className={css.body}>
					{children}
				</div>
			</div>
		)
	}
}
