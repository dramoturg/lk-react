import React from 'react';
import classnames from 'classnames';
import { Component } from 'react';
import { withTheme } from '@devexperts/react-kit/dist/utils/withTheme';

import doneTickIcon from './icon/done-tick.png';
import * as css from './theme/step.components.module.styl';

class RawStep extends Component {
	render() {
		const { theme, children, isCurrent, isCompleted, isDisabled, number } = this.props;

		const className = classnames(theme.container,{
			[theme.container_isDisabled]: isDisabled
		});

		const markerClassName = classnames(theme.marker,{
			[theme.marker_isCurrent]: isCurrent,
			[theme.marker_isCompleted]: isCompleted,
			[theme.marker_isDisabled]: isDisabled
		});
		return (
			<div className={className}>
				<div className={markerClassName}>
					{isCurrent && number}
					{isCompleted && (
						<img className={theme.icon} src={doneTickIcon} />
					)}
				</div>
				<div className={theme.label}>
					{children}
				</div>
			</div>
		)
	}
}

const theme = {
	...css,
};

export const Step = withTheme(Symbol(), theme)(RawStep);
