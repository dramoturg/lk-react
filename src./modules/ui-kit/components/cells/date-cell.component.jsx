import React, {Component} from 'react';
import {TableCell} from "@devexperts/react-kit/dist/components/Table/Table";
import {CalendarIcon} from "../icons/calendar.icon";
import css from './theme/icon-cell.component.module.styl';

export class DateCell extends Component {
	render() {
		const { children } = this.props;

		return (
			<TableCell>
				<div className={css.container}>
					<div className={css.icon}>
						<CalendarIcon />
					</div>
					{children}
				</div>
			</TableCell>
		)
	}
}
