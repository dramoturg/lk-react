import React, {Component} from 'react';
import {Col, Container, Row} from "reactstrap";
import {Selectbox} from "../selectbox/selectbox.component";
import {MenuItem} from "@devexperts/react-kit/dist/components/Menu/Menu";
import {Checkbox} from "../dx-checkbox/checkbox.component";
import {SelectboxAnchor} from "@devexperts/react-kit/dist/components/Selectbox/SelectboxAnchor";

import css from './theme/table-footer.component.module.styl';
import selectBoxTheme from './theme/selectbox.child.module.styl';

class Anchor extends React.Component {
	render() {
		const newProps = {
			...this.props,
			valueText: 'Применить к выбранным'
		};
		return (
			<SelectboxAnchor {...newProps} />
		)
	}
}

export class TableFooter extends Component {
	render() {
		return (
			<div className={css.container}>
				<Container fluid>
					{/*<Row>
						<Col xs={12}>
							<Selectbox theme={selectBoxTheme} Anchor={Anchor}>
								<MenuItem>
									<Checkbox value={true}>Жилые помещения</Checkbox>
								</MenuItem>
								<MenuItem>
									<Checkbox>Нежелые помещения</Checkbox>
								</MenuItem>
								<MenuItem>
									<Checkbox>Лестничные клетки</Checkbox>
								</MenuItem>
							</Selectbox>
						</Col>
					</Row>*/}
					<Row>
						<Col xs={6}>
							<div className={css.selected}>
								Выбрано 325 помещений
							</div>
							<div className={css.singlePrice}>
								Cтоимость за 1 помещение 30руб
							</div>

							<p>
								<a href="#">
									смотреть тарифы от 15 руб. за помещение
								</a>
							</p>
						</Col>
						<Col xs={6}>
							<div className={css.total}>
								<div className={css.totalCalc}>
									Стоимость 352 * 30р =
								</div>
								<div className={css.totalTotal}>
									<mark className={css.totalPrice}> 10 560</mark>
									<strong className={css.totalCurrency}>руб</strong>
								</div>
							</div>
						</Col>
					</Row>
				</Container>
			</div>
		)
	}
}
