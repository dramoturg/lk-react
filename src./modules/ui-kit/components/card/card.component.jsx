import React from 'react';
import { Component } from 'react';
import { withTheme } from '@devexperts/react-kit/dist/utils/withTheme';
import * as css from './theme/card.component.module.styl';

class RawStep extends Component {
	render() {
		const { theme, children, header, footer } = this.props;

		return (
			<div className={theme.container}>
				{header && (
					<div className={theme.header}>
						{header}
					</div>
				)}
				<div className={theme.body}>
					{children}
				</div>
				{footer && (
					<div className={theme.footer}>
						{footer}
					</div>
				)}
			</div>
		)
	}
}

const theme = {
	...css,
};

export const Card = withTheme(Symbol(), theme)(RawStep);
