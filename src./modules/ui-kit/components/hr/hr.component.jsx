import * as React from 'react';
import { Component } from 'react';
import { withTheme } from '@devexperts/react-kit/dist/utils/withTheme';
import * as css from './theme/hr.component.module.styl';

class RawHr extends Component {
	render() {
		const { theme } = this.props;

		return <hr className={theme.container} />;
	}
}

const theme = {
	...css,
};

export const Hr = withTheme(Symbol(), theme)(RawHr);
