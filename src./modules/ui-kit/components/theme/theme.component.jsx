import * as React from 'react';
import { ThemeProvider as ThemrProvider } from 'react-css-themr';
import { ThemeProvider } from '@devexperts/react-kit/dist/utils/withTheme';

import 'reset-css';
import './bootstrap.scss';
import './theme.component.styl';

import { CONTEXT_THEME } from '../config/theme.config';

export const Theme = props => {
	const theme = props.theme || CONTEXT_THEME;
	return (
		<ThemrProvider theme={theme}>
			<ThemeProvider theme={theme}>{props.children}</ThemeProvider>
		</ThemrProvider>
	);
};
