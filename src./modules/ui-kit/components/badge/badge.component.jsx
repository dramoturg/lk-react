import * as React from 'react';
import { Component } from 'react';
import { withTheme } from '@devexperts/react-kit/dist/utils/withTheme';
import * as css from './theme/badge.component.module.styl';

class RawBadge extends Component {
	render() {
		const { theme, children } = this.props;

		return (
			<div className={theme.container}>
				<div className={theme.back}>
					<svg>
						<path fillRule="evenodd"
							  d="M-0.000,-0.001 L145.000,-0.001 C145.000,-0.001 133.129,15.039 133.129,15.499 C133.129,15.773 145.000,30.999 145.000,30.999 L-0.000,30.999 L-0.000,-0.001 Z"/>
					</svg>
				</div>
				<div className={theme.label}>
					{children}
				</div>
			</div>
		)
	}
}

const theme = {
	...css,
};

export const Badge = withTheme(Symbol(), theme)(RawBadge);
