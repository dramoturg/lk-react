import React, {Fragment, Component} from 'react';
import cn from 'classnames';
import {Card} from "../../../ui-kit/components/card/card.component";
import {Button} from "../../../ui-kit/components/button/button.component";

import cardTheme from './theme/card.child.module.styl';
import buttonTheme from './theme/button.child.module.styl';

export class Tariff extends Component {

	render() {
		const {title, discount, children, isCustom} = this.props;

		const theme = {
			...cardTheme,
			container: cn(cardTheme.container, {
				[cardTheme.container_isCustom] : isCustom
			})
		};

		const header = (
			<Fragment>
				<span>{title}</span>
				{discount && <div className={cardTheme.discount}>-{discount}%</div> }
			</Fragment>
		);
		const footer = (
			<Button isPrimary={true} theme={buttonTheme}>
				Заказать
			</Button>
		);
		return (
			<Card theme={theme} footer={footer} header={header}>
				{children}
			</Card>
		);
	}
}
