import React from 'react';
import classnames from 'classnames';
import {Col, Collapse, Container, Form, FormGroup, Row} from "reactstrap";

import {Button} from "../../ui-kit/components/button/button.component";
import {ButtonIcon} from "../../ui-kit/components/button-icon/button-icon.component";
import {PlusIcon} from "../../ui-kit/components/icons/plus.icon";
import {SearchIcon} from "../../ui-kit/components/icons/search.icon";
import {MoveToNext2} from "../../ui-kit/components/icons/move-to-next2.icon";
import {Input} from "../../ui-kit/components/input/input.component";

import css from './theme/mass-preparation.module.styl';
import inputTheme from './theme/input.child.module.styl';


class Group extends React.Component {
	render() {
		const {isOpen} = this.props;

		return(
			<Collapse isOpen={isOpen}>
				<ul className={css.list}>
					<li className={css.item}>
						Санкт-Петербург, ул. Рязана Германа, д. 7, к.
						<div className={css.remove}>
							<ButtonIcon icon={<PlusIcon />}/>
						</div>
					</li>
					<li className={css.item}>
						Санкт-Петербург, ул. Рязанская, д. 7, к. 4, лит. А
						<div className={css.remove}>
							<ButtonIcon icon={<PlusIcon />}/>
						</div>
					</li>
					<li className={css.item}>
						Санкт-Петербург, ул. Рязанская, д. 7, к. 4, лит. Б
						<div className={css.remove}>
							<ButtonIcon icon={<PlusIcon />}/>
						</div>
					</li>
					<li className={css.item}>
						Санкт-Петербург, ул. Рязанская, д. 7, к. 4
						<div className={css.remove}>
							<ButtonIcon icon={<PlusIcon />}/>
						</div>
					</li>
				</ul>
			</Collapse>
		)
	}
}

export class MassPreparation extends React.Component {

	state = {
		expandedKeys: [1]
	}

	render() {
		const {expandedKeys} = this.state;
		const isExpand = key => expandedKeys.includes(key);

		return (
			<div className={css.container}>
				<Container fluid>
					<Row>
						<Col sm={12} lg={6} className={css.entered}>
							<Form>
								<FormGroup>
									<div className={css.searchIcon}>
										<SearchIcon />
									</div>
									<Input theme={inputTheme} placeholder="Введите адрес в соответствии с форматов КЛАДР" />
								</FormGroup>
							</Form>

							<ul className={css.list}>
								<li className={css.item}>
									<mark>Санкт</mark>-Петербург, ул. Рязана Германа, д. 7, к.
									<div className={css.remove}>
										<ButtonIcon icon={<PlusIcon />}/>
									</div>
								</li>
								<li className={`${css.item} ${css.item_selected}`}>
									<mark>Санкт</mark>-Петербург, ул. Рязанская, д. 7, к. 4, лит. А
									<div className={css.remove}>
										<ButtonIcon icon={<PlusIcon />}/>
									</div>
								</li>
								<li className={css.item}>
									<mark>Санкт</mark>-Петербург, ул. Рязанская, д. 7, к. 4, лит. Б
									<div className={css.remove}>
										<ButtonIcon icon={<PlusIcon />}/>
									</div>
								</li>
								<li className={css.item}>
									<mark>Санкт</mark>-Петербург, ул. Рязанская, д. 7, к. 4
									<div className={css.remove}>
										<ButtonIcon icon={<PlusIcon />}/>
									</div>
								</li>
							</ul>
						</Col>
						<Col sm={12} lg={6} className={css.approved}>
							<p>Подтверждение адреса</p>
							<div className={css.group}>
								<Button isBlock={true} isFlat={true} onClick={this.onClick(1)}>
									<div className="d-flex align-items-center">
										<div className={css.t}>Выбирите из предложенных вариантов</div>
										<div className={css.toggleIcon}>
											<MoveToNext2 />
										</div>
									</div>
								</Button>
								<Group isOpen={isExpand(1)} />
							</div>

							<div className={css.group}>
								<Button isBlock={true} isFlat={true} onClick={this.onClick(2)}>
									<div className="d-flex align-items-center">
										<div className={css.t}>Выбирите из предложенных вариантов</div>
										<div className={css.toggleIcon}>
											<MoveToNext2 />
										</div>
									</div>
								</Button>
								<Group isOpen={isExpand(2)} />
							</div>

							<div className={css.group}>
								<Button isBlock={true} isFlat={true} onClick={this.onClick(3)}>
									<div className="d-flex align-items-center">
										<div className={css.t}>Выбирите из предложенных вариантов</div>
										<div className={css.toggleIcon}>
											<MoveToNext2 />
										</div>
									</div>
								</Button>
								<Group isOpen={isExpand(3)} />
							</div>

						</Col>
					</Row>
				</Container>
			</div>
		)
	}

	onClick = key => () => {
		const {expandedKeys} = this.state;
		let newValue;
		if (expandedKeys.includes(key)) {
			newValue = expandedKeys.filter(item => item !== key);
		} else {
			newValue = [
				...expandedKeys,
				key
			]
		}
		this.setState({
			expandedKeys: newValue
		})
	}
}
