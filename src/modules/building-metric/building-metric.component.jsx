import * as React from 'react';
import { Component } from 'react';
import { withTheme } from '@devexperts/react-kit/dist/utils/withTheme';
import * as css from './theme/building-metric.module.styl';

class RawBuildingMetrics extends Component {
	render() {
		const { metrics, theme } = this.props;

		return (
			<div className={theme.container}>
				{metrics.length > 0 && metrics.map(({title, value}, index) => {
					return (
						<div key={`building-metric-${index}`} className={theme.item}>
							<div className={theme.title}>
								{title}
							</div>
							<div className={theme.value}>
								{value}
							</div>
						</div>
					)
				})}
			</div>
		)
	}
}

const theme = {
	...css,
};

export const BuildingMetrics = withTheme(Symbol(), theme)(RawBuildingMetrics);
