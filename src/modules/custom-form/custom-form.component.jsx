import React from 'react';
import {Input} from "../ui-kit/components/input/input.component";
import css from './theme/custom-form.component.module.styl';
import inputTheme from './theme/input.child.module.styl';

export class CustomForm extends React.Component {
	render() {
		return (
			<form className={css.container}>
				<div className={css.row}>
					<Input theme={inputTheme} value={250} />
					<div className={css.label}>Помещений</div>
					<div className={css.price}>
						30р
					</div>
				</div>
				<div className={css.row}>
					<Input theme={inputTheme} value={250} />
					<div className={css.label}>Помещений</div>
					<div className={css.price}>
						30р
					</div>
				</div>
				<div className={css.row}>
					<Input theme={inputTheme} value={3} />
					<div className={css.label}>Собраний</div>
					<div className={css.price}>
						3000 р
					</div>
				</div>
				<div className={css.summary}>
					<div className={css.label}>Итого:</div>
					7 540 руб
				</div>
			</form>
		);
	}
}
