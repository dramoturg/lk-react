import React from 'react';
import {StatusType} from "../../ui-kit/components/status/status.component";
export const data = [
	{

		isChecked: false,
		index: 1,
		address: <div>г. Санкт-Петербург, Московское ш., д. 30, корп. 2, лит. А, кв. 1</div>,
		unloadDate: '10.05.2018',
		count: 15,
		status: {
			type: StatusType.Pending,
			text: 'Ожидает оплаты'
		},
		comment: 'Скорее всего нет данных'
	},
	{

		isChecked: false,
		index: 2,
		unloadDate: '10.05.2018',
		address: <div>г. Санкт-Петербург, Московское ш., д. # 30, корп. 2, лит. А, кв. 1</div>,
		count: 15,
		selected: 20,
		status: {
			type: StatusType.Prepare,
			text: 'Реестр готовится'
		},
		comment: 'Скорее всего нет данных'
	},
	{

		isChecked: false,
		index: 3,
		unloadDate: '10.05.2018',
		address: <div>г. Санкт-Петербург, Московское ш., д. # 30,  корп. 2, лит. А, кв. 1</div>,
		count: 15,
		selected: 20,
		status: {
			type: StatusType.Ready,
			text: 'Скачать'
		},
		comment: 'Скорее всего нет данных'
	}
];
