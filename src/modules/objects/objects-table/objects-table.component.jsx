import React from 'react';
import {Column, RenderTable} from "../../ui-kit/components/render-table/render-table.component";
import {TableCell} from "@devexperts/react-kit/dist/components/Table/Table";
import {Location} from "../../ui-kit/components/location/location.component";
import {Status} from "../../ui-kit/components/status/status.component";
import {Checkbox} from "../../ui-kit/components/dx-checkbox/checkbox.component";
import {Date} from "../../ui-kit/components/date/date.component";
import {RoomFiler} from "../../ui-kit/components/room-filter/room-filter.component";
import {data} from './objects-table.fixture';
import {QuotationIcon} from "../../ui-kit/components/icons/quotation.icon";
import css from './theme/objects-table.component.module.styl';


const cellRenderer = f => cellData => <TableCell>{f(cellData)}</TableCell>;
const mobileRenderer = f => (cellData, dataKey) => <div className={`${css.item} ${css[`item_${dataKey}`]}`}>{f(cellData)}</div>;
const blankRenderer = () => null;
const commentRenderer =cellData => {
	return (
		<div className={css.commentItem}>
			<div className="q-icon">
				<QuotationIcon/>
			</div>
			{cellData}
		</div>
	)
};
const simpleRenderer = cellData => <div>{cellData}</div>;
const dateRenderer = cellData => {
	return (
		<Date>{cellData}</Date>
	)
}
const locationRenderer = cellData => {
	return (
		<Location>{cellData}</Location>
	)
};

const filterRenderer = cellData => {
	return (
		<RoomFiler selected={100} total={352} />
	)
};

const statusRenderer = status => (
	<Status type={status.type}>
		{status.text}
	</Status>
);

const dataChangesTypeRenderer = cellData => {
	return (
		<a href="#">
			Подписаться <br />
			на обновления
		</a>
	)
}

const isCheckedRenderer = value => <Checkbox value={value}/>;

export class ObjectsTable extends React.Component {

	render() {
		const {isVisibleSchedules} = this.props;

		return (
			<RenderTable data={data}>
				<Column
					cellRenderer={cellRenderer(isCheckedRenderer)}
					headerRenderer={this.checkHeaderRenderer}
					mobileCellRenderer={mobileRenderer(isCheckedRenderer)}
					dataKey={'isChecked'}
				/>
				<Column dataKey={'index'}
				        label={'#'}
				        mobileCellRenderer={mobileRenderer(blankRenderer)}
				        cellRenderer={cellRenderer(simpleRenderer)}/>
				<Column dataKey={'unloadDate'}
				        label={'Дата выгрузки'}
				        mobileCellRenderer={mobileRenderer(dateRenderer)}
				        cellRenderer={cellRenderer(dateRenderer)}/>
				<Column dataKey={'address'}
				        label={'Адрес'}
				        mobileCellRenderer={mobileRenderer(locationRenderer)}
				        cellRenderer={cellRenderer(locationRenderer)}/>
				<Column dataKey={'count'}
				        label={'Кол-во помещений'}
				        mobileCellRenderer={mobileRenderer(filterRenderer)}
				        cellRenderer={cellRenderer(filterRenderer)}/>
				<Column dataKey={'status'}
				        label={'Статус'}
				        mobileCellRenderer={mobileRenderer(statusRenderer)}
				        cellRenderer={cellRenderer(statusRenderer)}/>

				{isVisibleSchedules && (
					<Column dataKey={'status'}
							label={<div>Изменение данных о <br/> правах</div>}
							mobileCellRenderer={mobileRenderer(dataChangesTypeRenderer)}
							cellRenderer={cellRenderer(dataChangesTypeRenderer)}/>
				)}

				<Column dataKey={'comment'}
				        label={'Комментарий'}
				        mobileCellRenderer={mobileRenderer(commentRenderer)}
				        cellRenderer={cellRenderer(simpleRenderer)}/>
			</RenderTable>
		)
	}

	checkHeaderRenderer = () => {
		return (
			<div>
				<Checkbox checked={false} />
			</div>
		);
	};
}
