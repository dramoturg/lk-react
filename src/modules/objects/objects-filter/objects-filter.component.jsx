import React from 'react';
import {Checkbox} from "../../ui-kit/components/dx-checkbox/checkbox.component";
import {SelectboxAnchor} from "@devexperts/react-kit/dist/components/Selectbox/SelectboxAnchor";
import {Selectbox} from "../../ui-kit/components/selectbox/selectbox.component";
import {MenuItem} from "@devexperts/react-kit/dist/components/Menu/Menu";

import css from './theme/objects-filter.component.module.styl';
import selectBoxTheme from './theme/selectbox.child.module.styl';

class Anchor extends React.Component {
	render() {
		const newProps = {
			...this.props,
			valueText: 'Применить к выбранным'
		};
		return (
			<SelectboxAnchor {...newProps} />
		)
	}
}

export class ObjectsFilter extends React.Component {
	render() {
		return (
			<div className={css.container}>
				<Selectbox theme={selectBoxTheme} Anchor={Anchor}>
					<MenuItem>
						<Checkbox value={true}>Жилые помещения</Checkbox>
					</MenuItem>
					<MenuItem>
						<Checkbox>Нежелые помещения</Checkbox>
					</MenuItem>
					<MenuItem>
						<Checkbox>Лестничные клетки</Checkbox>
					</MenuItem>
				</Selectbox>
			</div>
		);
	}
}
