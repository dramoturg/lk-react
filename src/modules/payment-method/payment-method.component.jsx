import React from 'react';
import classnames from 'classnames';
import {Checkbox} from "../ui-kit/components/dx-checkbox/checkbox.component";

import i1Logo from './images/i1.png';
import i2Logo from './images/i2.png';
import i3Logo from './images/i3.png';

import css from './theme/payment-method.component.module.styl';
import checkboxTheme from './theme/checkbox.child.module.styl';

export const AvailableType = {
	Sberbank: 'sberbank',
	Yanex: 'yandex',
	Transfer: 'transfer'
};

const imageTypeMap = {
	[AvailableType.Sberbank]: i1Logo,
	[AvailableType.Yanex]: i2Logo,
	[AvailableType.Transfer]: i3Logo
};

export class PaymentMethod extends React.Component {

	state = {
		value: this.props.isChecked
	}

	render() {
		const {title, children, type} = this.props;
		const {value} = this.state;
		const image = imageTypeMap[type];
		const classNames = classnames(css.container, {
			[css.container_isChecked]: value
		})
		return (
			<div className={classNames}>
				<Checkbox value={value} theme={checkboxTheme} onValueChange={this.handleChange}/>
				<div className={css.image}>
					<img src={image}  title={title}/>
				</div>
				{title && (
					<div className={css.title}>
						{title}
					</div>
				)}
				<div className={css.body}>
					{children}
				</div>
			</div>
		)
	}

	handleChange = () => {
		this.setState({
			value: !this.state.value
		})
	}
}
