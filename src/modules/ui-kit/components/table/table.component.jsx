import React, {Component} from 'react';
import { Table as DXCTable } from '@devexperts/react-kit/dist/components/Table/Table';
import css from './theme/table.component.module.styl';

export * from '@devexperts/react-kit/dist/components/Table/Table';
export class Table extends Component {
	render() {
		return(
			<div className={css.wrapper}>
				<DXCTable {...this.props} />
			</div>
		)
	}

}
