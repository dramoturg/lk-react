import * as React from 'react';
import theme from './theme/icon-wrapper.component.module.styl';

export const IconWrapper = props => (
	<i className={theme.container}>{props.children}</i>
);

