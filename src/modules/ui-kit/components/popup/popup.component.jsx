import React from 'react';
import { memoize } from '@devexperts/utils/dist/function/memoize';
import { Popup as DXCPopup } from '@devexperts/react-kit/dist/components/Popup/Popup';
import {ButtonIcon} from "../button-icon/button-icon.component";
import {PlusIcon} from "../icons/plus.icon";
import * as css from './popup.component.module.styl';

export * from '@devexperts/react-kit/dist/components/Popup/Popup';


const getRoot = memoize(() => {
	const root = document.getElementById('root');
	return root || undefined;
});

export class Popup extends React.Component {
	container = getRoot();

	componentWillReceiveProps(nextProps) {
		if (this.container && this.props.isOpened !== nextProps.isOpened) {
			const xPos = this.container.scrollLeft;
			const yPos = this.container.scrollTop;

			if (nextProps.isOpened) {
				this.container.scrollTop = yPos;
				this.container.scrollLeft = xPos;
			} else {
				window.scrollTo(xPos, yPos);
			}
		}
	}

	render() {
		const {header} = this.props;
		const newHeader = (
			<div className={css.headerContent}>
				{header}
				<ButtonIcon onClick={this.props.onRequestClose} icon={<PlusIcon />} theme={{
					container: css.buttonClose
				}}/>
			</div>
		);
		return (
			<DXCPopup container={this.container} {...this.props} header={newHeader}/>
		);
	}
}
