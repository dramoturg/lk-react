import React, {Component} from 'react';
import {TableCell} from "@devexperts/react-kit/dist/components/Table/Table";
import {PinIcon} from "../icons/pin.icon";
import css from './theme/icon-cell.component.module.styl';

export class PinCell extends Component {
	render() {
		const { children } = this.props;

		return (
			<TableCell>
				<div className={css.container}>
					<div className={css.icon}>
						<PinIcon />
					</div>
					{children}
				</div>
			</TableCell>
		)
	}
}
