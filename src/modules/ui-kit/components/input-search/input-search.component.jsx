import * as React from 'react';
import { Component } from 'react';
import { withTheme } from '@devexperts/react-kit/dist/utils/withTheme';
import { Input } from '../input/input.component';
import { SearchIcon } from '../icons/search.icon';
import css from './theme/input-search.component.module.styl';

const iconSearch = <SearchIcon />;

class RawInputSearch extends Component {
	render() {
		const { theme, placeholder } = this.props;

		return (
			<Input {...this.props} theme={theme} placeholder={placeholder}>
				<i className={theme.icon}>{iconSearch}</i>
			</Input>
		);
	}
}

const theme = {
	...css,
};
export const InputSearch = withTheme(Symbol(), theme)(RawInputSearch);
