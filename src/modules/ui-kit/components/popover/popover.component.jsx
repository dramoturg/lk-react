import * as React from 'react';
import { memoize } from '@devexperts/utils/dist/function/memoize';
import { Popover as DXCPopover } from '@devexperts/react-kit/dist/components/Popover/Popover';

export * from '@devexperts/react-kit/dist/components/Popover/Popover';

const getRoot = memoize(() => {
	const root = document.getElementById('root');
	return root || undefined;
});

export const Popover = (props) => <DXCPopover container={getRoot()} {...props} />;
