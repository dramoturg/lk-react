import React from 'react';
import css from './theme/button.component.module.styl';
import {
	Button as DXCButton,
	BUTTON
} from '@devexperts/react-kit/dist/components/Button/Button';
import { Component } from 'react';
import classNames from 'classnames';
import { withTheme } from '@devexperts/react-kit/dist/utils/withTheme';

class RawButton extends Component {
	render() {
		const { isExtra, isBlock, ...props } = this.props;
		let theme = props.theme;
		if (isExtra) {
			theme = {
				...theme,
				container: classNames(theme.container, theme.container_isExtra),
			};
		}
		if (isBlock) {
			theme = {
				...theme,
				container: classNames(theme.container, theme.container_isBlock),
			};
		}

		return <DXCButton {...props} theme={theme} />;
	}
}
export const Button = withTheme(BUTTON, css)(RawButton);
