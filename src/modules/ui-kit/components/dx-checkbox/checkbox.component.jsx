import * as React from 'react';
import { Component } from 'react';
import * as classNames from 'classnames';
import { withTheme } from '@devexperts/react-kit/dist/utils/withTheme';
import {
	Checkbox as DXCCheckbox,
	CHECKBOX
} from '@devexperts/react-kit/dist/components/Checkbox/Checkbox';

//import { CheckboxTickIcon } from '../icons/checkbox-tick.icon';

import css from './checkbox.component.module.styl';
import doneTickIcon from './icons/done-tick.png';
export * from '@devexperts/react-kit/dist/components/Checkbox/Checkbox';

const iconCheckboxTick = (
	<img src={doneTickIcon} />
);

class RawCheckbox extends Component{
	static defaultProps = {
		checkMark: iconCheckboxTick,
	};

	render() {
		const { value, isDisabled, children, checkMark, ...props } = this.props;
		let theme = props.theme;

		theme = {
			...theme,
			view: classNames(theme.view, {
				[theme.view_isChecked]: value,
				[theme.view_isCheckedDisabled]: value && isDisabled,
			}),
		};

		return (
			<label className={theme.container}>
				<div className={theme.checkboxContainer}>
					<DXCCheckbox {...this.props} theme={theme} checkMark={checkMark} />
				</div>
				{children && <span className={theme.text}>{children}</span>}
			</label>
		);
	}
}

export const Checkbox = withTheme(CHECKBOX, css)(RawCheckbox);
