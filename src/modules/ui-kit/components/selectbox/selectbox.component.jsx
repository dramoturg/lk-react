import * as React from 'react';
import { Selectbox as DXCSelectbox } from '@devexperts/react-kit/dist/components/Selectbox/Selectbox';
import './theme/selectbox.component.module.styl';
import { Popover } from '../popover/popover.component';
import {MoveToNext2} from "../icons/move-to-next2.icon";
export * from '@devexperts/react-kit/dist/components/Selectbox/Selectbox';

const iconArrow = <MoveToNext2 />;

export const Selectbox = props => (
	<DXCSelectbox caretIcon={iconArrow} Popover={Popover} {...props} />
);
