import React, {Fragment} from 'react';
import {
	Table,
	TableHead,
	TableCell,
	TableBody,
	TableRow
} from "../../../ui-kit/components/table/table.component";

import css from './theme/render-table.component.module.styl';

export class RenderTable extends React.Component {

	render() {
		const {data, children} = this.props;

		return (
			<Fragment>
				<div className='d-none d-xl-block'>
					<Table>
						<TableHead>
							<TableRow>
								{React.Children.map(children, child => {
									if (child === null) return child;
									const {props: {label, headerRenderer}} = child;
									return <TableCell>{headerRenderer ? headerRenderer() : label}</TableCell>
								})}
							</TableRow>
						</TableHead>
						<TableBody>
							{data.map((rowData, index) => {
								return (
									<TableRow key={`body-row${index}`}>
										{React.Children.map(children, child => {
											if (child === null) return child;
											const {props: {cellRenderer, dataKey}} = child;
											return cellRenderer(rowData[dataKey]);
										})}
									</TableRow>
								)
							})}
						</TableBody>
					</Table>
				</div>
				<div className='d-xl-none'>
					{data.map((rowData, index) => {
						return (
							<div key={`body-row-mobile-${index}`} className={css.item}>
								{React.Children.map(children, child => {
									if (child === null) return child;
									const {props: {mobileCellRenderer, dataKey}} = child;
									return mobileCellRenderer ? mobileCellRenderer(rowData[dataKey], dataKey) : rowData[dataKey]
								})}
							</div>
						)
					})}
				</div>
			</Fragment>
		);
	}
}

export class Column extends React.Component {
	render() {
		return null;
	}
}
