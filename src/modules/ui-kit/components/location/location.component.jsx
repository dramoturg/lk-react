import React, {Component} from 'react';
import {PinIcon} from "../icons/pin.icon";
import { withTheme } from '@devexperts/react-kit/dist/utils/withTheme';
import css from './theme/location.component.module.styl';

export class RawLocation extends Component {
	render() {
		const { children } = this.props;

		return (
			<div className={css.container}>
				<div className={css.icon}>
					<PinIcon />
				</div>
				{children}
			</div>
		)
	}
}

const theme = {
	...css,
};

export const Location = withTheme(Symbol(), theme)(RawLocation);
