import {
	ButtonIcon as DXCButtonIcon,
} from '@devexperts/react-kit/dist/components/ButtonIcon/ButtonIcon';
import './theme/button-icon.component.module.styl';
export * from '@devexperts/react-kit/dist/components/ButtonIcon/ButtonIcon';
export const ButtonIcon = DXCButtonIcon;
