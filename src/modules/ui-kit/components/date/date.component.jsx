import React, {Component} from 'react';
import {CalendarIcon} from "../icons/calendar.icon";
import { withTheme } from '@devexperts/react-kit/dist/utils/withTheme';
import css from './theme/date.component.module.styl';

export class RawDate extends Component {
	render() {
		const { children } = this.props;

		return (
			<div className={css.container}>
				<div className={css.icon}>
					<CalendarIcon />
				</div>
				{children}
			</div>
		)
	}
}

const theme = {
	...css,
};

export const Date = withTheme(Symbol(), theme)(RawDate);
