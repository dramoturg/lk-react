import * as React from 'react';
import { Component } from 'react';
import { withTheme } from '@devexperts/react-kit/dist/utils/withTheme';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import {Step} from "../step/step.component";
import iconArrow from './icons/arrow.png';

import * as css from './theme/page.component.module.styl';

class RawPage extends Component {
	render() {
		const { children, theme, title, step: currentStep, withSteps } = this.props;
		const steps = withSteps ? [
			'Ввод адреса',
			'Выбор помещения',
			<strong>Оплата</strong>,
			'Мастер подготовки реестра'
		] : [];
		return (
			<div className={theme.container}>
				<div className={theme.wrapper}>
					<div className={theme.breadcrumbs}>
						<Breadcrumb>
							<BreadcrumbItem>
								<a href="#">Главная</a>
								<img src={iconArrow} />
							</BreadcrumbItem>
							<BreadcrumbItem>
								<a href="#">Все реестры</a>
								<img src={iconArrow} />
							</BreadcrumbItem>
							<BreadcrumbItem active>Мастер подготовки реестра</BreadcrumbItem>
						</Breadcrumb>
					</div>

					<div className={theme.pageTitle}>
						{title}
					</div>
					{steps.length > 0 && (
						<div className={theme.steps}>
							{steps.map((step, index) => {
								return (
									<Step number={index + 1} key={index} isDisabled={currentStep  < index} isCurrent={currentStep === index} isCompleted={currentStep > index}>
										{step}
									</Step>
								)
							})}
						</div>
					)}
				</div>

				<div className={theme.content}>
					{children}
				</div>

			</div>
		)
	}
}

const theme = {
	...css,
};

export const Page = withTheme(Symbol(), theme)(RawPage);
