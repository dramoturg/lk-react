import * as React from 'react';
import {IconWrapper} from "../icon-wrapper/icon-wrapper.component";
import pdfIcon from './images/pdf.png';

export const PdfIcon = () => (
	<IconWrapper>
		<img src={pdfIcon} />
	</IconWrapper>
);
