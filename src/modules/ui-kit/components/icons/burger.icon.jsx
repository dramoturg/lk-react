import * as React from 'react';
import {IconWrapper} from "../icon-wrapper/icon-wrapper.component";

export const BurgerIcon = () => (
	<IconWrapper>
		<svg width="21px" height="16px">
			<defs>
				<filter id="Filter_0">
					<feFlood floodColor="rgb(39, 176, 49)" floodOpacity="1" result="floodOut" />
					<feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut" />
					<feBlend mode="normal" in="compOut" in2="SourceGraphic" />
				</filter>

			</defs>
			<g filter="url(#Filter_0)">
				<path fillRule="evenodd"  fill="rgb(39, 176, 49)"
					  d="M-0.000,16.000 L-0.000,14.015 L21.000,14.015 L21.000,16.000 L-0.000,16.000 ZM-0.000,7.015 L21.000,7.015 L21.000,9.000 L-0.000,9.000 L-0.000,7.015 ZM-0.000,0.016 L21.000,0.016 L21.000,2.000 L-0.000,2.000 L-0.000,0.016 Z"/>
			</g>
		</svg>
	</IconWrapper>
);
