import * as React from 'react';
import {IconWrapper} from "../icon-wrapper/icon-wrapper.component";
import pngIcon from './images/png.png';

export const PngIcon = () => (
	<IconWrapper>
		<img src={pngIcon} />
	</IconWrapper>
);
