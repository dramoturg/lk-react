import * as React from 'react';
import {IconWrapper} from "../icon-wrapper/icon-wrapper.component";
import docIcon from './images/doc.png';

export const DocIcon = () => (
	<IconWrapper>
		<img src={docIcon} />
	</IconWrapper>
);
