import * as React from 'react';
import {IconWrapper} from "../icon-wrapper/icon-wrapper.component";
import xlsIcon from './images/xls.png';

export const XlsIcon = () => (
	<IconWrapper>
		<img src={xlsIcon} />
	</IconWrapper>
);
