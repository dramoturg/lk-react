import * as React from 'react';
import {IconWrapper} from "../icon-wrapper/icon-wrapper.component";

export const MoveToNext2 = () => (
	<IconWrapper>
		<svg viewBox="0 0 24 24">
			<polyline strokeLinecap="round" strokeLinejoin="round" strokeWidth="5px" fill="none" stroke="currentColor"  points="20.59 7.66 11.9 16.34 3.41 7.86"/>
		</svg>
	</IconWrapper>
);
