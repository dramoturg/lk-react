import * as React from 'react';
import {IconWrapper} from '@devexperts/react-kit/dist/components/icon-wrapper/icon-wrapper.component';

export const MoveToNextIcon = () => (
	<IconWrapper>
		<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAJCAYAAADtj3ZXAAAA20lEQVQokXXPoS+FURiA8d/HZqNIGklQbIKo2PwFKAraq7jZ7twqSTbBOTbaDeI1o5k/QdMESVEljuBjn8+9b3ze8zw7b5VS2sI53rATEQ9GTM75ED28YLVKKT1hod6/YyMi7lpShRN0GnhvDM8NMIlBznm9IY7joiXCa5VSmsM95huLT+ziCn1stsQ+tqtSipzzLG6x2Hr0iKUWO8N+RHxUpZSf783UgWWj5xgHEVHgV64D07jByhCxFxFHTfBHrgNTGGCtgTsRcdqu/ZPrwAS6vu+9jIjrYTd8AfTUTF1nHlQbAAAAAElFTkSuQmCC" />
	</IconWrapper>
);
