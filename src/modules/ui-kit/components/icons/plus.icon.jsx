import * as React from 'react';
import {IconWrapper} from '@devexperts/react-kit/dist/components/icon-wrapper/icon-wrapper.component';

export const PlusIcon = () => (
	<IconWrapper>
		<svg
			width="10px" height="10px">
			<path fillRule="evenodd"  fill="currentColor"
			      d="M9.525,8.394 L8.394,9.525 L5.000,6.131 L1.606,9.525 L0.474,8.393 L3.868,5.000 L0.474,1.606 L1.606,0.474 L4.999,3.868 L8.394,0.474 L9.525,1.605 L6.131,5.000 L9.525,8.394 Z"/>
		</svg>
	</IconWrapper>
);
