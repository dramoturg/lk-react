import React from 'react';
import { Component } from 'react';
import cn from 'classnames';
import { withTheme } from '@devexperts/react-kit/dist/utils/withTheme';
import css from './theme/link-doc.component.module.styl';
import {Link} from "@devexperts/react-kit/dist/components/Link/Link";
import {DocIcon} from "../icons/doc.icon";
import {XlsIcon} from "../icons/xls.icon";
import {PngIcon} from "../icons/png.icon";
import {PdfIcon} from "../icons/pfd.icon";

export const DocType = {
	Doc: 'doc',
	Xls: 'xls',
	Pdf: 'pdf',
	Png: 'png'
};

const LinkDocIconMap = {
	[DocType.Doc]: DocIcon,
	[DocType.Xls]: XlsIcon,
	[DocType.Pdf]: PdfIcon,
	[DocType.Png]: PngIcon
}

class RawLinkDoc extends Component {
	render() {
		const { theme, type, children } = this.props;
		const classname = cn(theme.container, theme[`container_${type}`]);
		const Icon = LinkDocIconMap[type];
		return (
			<div className={classname}>
				<div className={theme.icon}>
					<Icon />
				</div>
				<Link href="#">
					{children}
				</Link>
			</div>
		)
	}
}

const theme = {
	...css,
};

export const LinkDoc = withTheme(Symbol(), theme)(RawLinkDoc);
