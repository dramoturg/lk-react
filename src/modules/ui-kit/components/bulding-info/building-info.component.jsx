import React from 'react';
import css from './theme/bulding-info.component.module.styl';
import {PinIcon} from "../icons/pin.icon";
import {SaveFileIcon} from "../icons/save-file.icon";
import {WritingIcon} from "../icons/whriting.icon";
import {ButtonIcon} from "../button-icon/button-icon.component";
import {BuildingMetrics} from "../../../building-metric/building-metric.component";

import buildingMetricTheme from './theme/building-metric.child.module.styl';

export class BuildingInfo extends React.Component {
	render() {
		const {metricks = [], header, children} = this.props;
		return (
			<div className={css.container}>
				<div className={css.adress}>
					<div className={css.adressIcon}>
						<PinIcon />
					</div>
					{header}
					<div className={css.controls}>
						<ButtonIcon icon={<WritingIcon />} />
						<ButtonIcon icon={<SaveFileIcon />}/>
					</div>
				</div>
				<BuildingMetrics metrics={metricks} theme={buildingMetricTheme} />
				{children}
			</div>
		);
	}
}
