import * as React from 'react';
import { Component } from 'react';
import { withTheme } from '@devexperts/react-kit/dist/utils/withTheme';
import {Menu, MenuItem} from "@devexperts/react-kit/dist/components/Menu/Menu";
import {Popover, PopoverAlign} from "../../../ui-kit/components/popover/popover.component";
import {Button} from "../button/button.component";
import {Checkbox} from "../dx-checkbox/checkbox.component"

import * as css from './theme/room-filter.component.module.styl';
import * as buttonTheme from './theme/button.child.module.styl';
import * as menuTheme from './theme/menu.child.module.styl';
import * as menuItemTheme from './theme/menu-item.child.module.styl';
import {MoveToNext2} from "../icons/move-to-next2.icon";

class RawRomFilter extends Component {
	anchor;

	state = {
		isOpened: false,
	}

	render() {
		const { selected, total } = this.props;
		const { isOpened } = this.state;
		return (
			<div className={theme.container}>

				<div className={theme.selected}>
					<div className={theme.label}>Помещений</div>
					<div className={theme.of}>
						{selected} из {total}
					</div>
				</div>

				<Button theme={buttonTheme} isFlat={true} ref={el => (this.anchor = el)} onClick={this.handleClick}>
					<div className={theme.anchorInner}>
						Выбрать
						<div className={theme.toggleIcon}>
							<MoveToNext2 />
						</div>
					</div>

					<Popover
						isOpened={isOpened}
						closeOnClickAway={true}
						onRequestClose={this.handleRequestClose}
						hasArrow={false}
						anchor={this.anchor}>

						<Menu theme={menuTheme}>
							<MenuItem theme={menuItemTheme}>
								<Checkbox value={true}>
									Выбрать все
								</Checkbox>
							</MenuItem>
							<MenuItem theme={menuItemTheme}>
								<Checkbox>
									Выбрать жилые помещения
								</Checkbox>
							</MenuItem>
							<MenuItem theme={menuItemTheme}>
								<Checkbox>
									Выбрать нежелые помещения
								</Checkbox>
							</MenuItem>
							<MenuItem theme={menuItemTheme}>
								<Checkbox>
									Выбрать все лестничные клетки, <br/>
									чердаки, подвалы
								</Checkbox>
							</MenuItem>
							<MenuItem theme={menuItemTheme}>
								<Checkbox>
									Выбор по номерам помещений
								</Checkbox>
							</MenuItem>
						</Menu>

					</Popover>
				</Button>
			</div>
		)
	}

	handleClick = () => {
		this.setState({
			isOpened: !this.state.isOpened,
		});
	};

	handleRequestClose = () => {
		this.setState({
			isOpened: false,
		});
	};
}

const theme = {
	...css,
};

export const RoomFiler = withTheme(Symbol(), theme)(RawRomFilter);
