import React from 'react';
import cn from "classnames";
import { Component } from 'react';
import { withTheme } from '@devexperts/react-kit/dist/utils/withTheme';
import {MoveToNext2} from "../icons/move-to-next2.icon";
import {Button} from "../button/button.component";
import css from './theme/button-navigation.module.styl';


class RawButtonNavigation extends Component {
	render() {
		const { isNext, children, theme, ...buttonProps } = this.props;
		const innerClassName = cn(theme.inner, {
			[theme.inner_next]: isNext
		});
		return (
			<Button {...buttonProps} theme={theme}>
				<div className={innerClassName}>
					<div className={theme.icon}>
						<MoveToNext2/>
					</div>
					{children}
				</div>
			</Button>
		)
	}
}

const theme = {
	...css,
};

export const ButtonNavigation = withTheme(Symbol(), theme)(RawButtonNavigation);
