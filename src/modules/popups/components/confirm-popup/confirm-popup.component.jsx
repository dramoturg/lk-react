import React, {Fragment} from 'react';
import {Popup} from "../../../ui-kit/components/popup/popup.component";
import * as theme from './theme/confirm-popup.component.module.styl';
import {Checkbox} from "../../../ui-kit/components/dx-checkbox/checkbox.component";
import {Button} from "../../../ui-kit/components/button/button.component";
import {Alert} from "reactstrap";

export class ConfirmPopup extends React.Component {

	render() {
		const {isOpened, onRequestClose} = this.props;
		const header = (
			<Fragment>
				<mark>Подтверждение заказа </mark>
			</Fragment>
		);

		const footer = (
			<Fragment>
				<Button isPrimary={true}>
					Создать заказ
				</Button>
				<span style={{
					padding: '0 10px'
				}} />
				<Button isFlat={true} onClick={onRequestClose}>
					<mark>Отмена</mark>
				</Button>
			</Fragment>
		);

		return (
			<Popup theme={theme} isModal={true} isOpened={isOpened} header={header} footer={footer} onRequestClose={onRequestClose}>
				<div className={theme.container}>
					<p>
						По текущему заказу будет выполнено 10 запросов
						на предоставление сведений из Росреестра
					</p>
					<Alert color='secondary'>
						<p>
							Стоимость выполнение данного заказа составляет <mark>200</mark> руб.
						</p>
					</Alert>
					<Checkbox value={true}>
						Списать денежные средства автоматически
						сразу после пополнения счета
					</Checkbox>
					<div className={theme.balance}>
						<div className={theme.balanceLabel}>БАЛАНС:</div>
						<div className={theme.summ}> <mark>0,00 </mark></div>
						<div className={theme.currency}> руб.</div>
						<Button isExtra={true}>Пополнить</Button>
					</div>
				</div>
			</Popup>
		)
	}
}
