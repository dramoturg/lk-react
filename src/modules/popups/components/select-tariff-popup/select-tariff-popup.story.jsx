import React from "react";
import { storiesOf } from "@storybook/react";
import {SelectTariffPopup} from "./select-tariff.popup.component";
import {Theme} from "../../../ui-kit/components/theme/theme.component";


storiesOf("Popup", module)
	.add("Select Tariff", () => (
		<Theme>
			<SelectTariffPopup />
		</Theme>
	));
