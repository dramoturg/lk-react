import React, {Fragment} from 'react';
import {withTheme} from '@devexperts/react-kit/dist/utils/withTheme';
import {Popup} from "../../../ui-kit/components/popup/popup.component";
import * as theme from './theme/select-tariff.popup.component.module.styl';
import {Button} from "../../../ui-kit/components/button/button.component";
import {Card, CardTitle, CardBody, CardFooter} from "reactstrap";
import {SignIcon} from "../../../ui-kit/components/icons/sign.icon";

export class RawSelectTariffPopup extends React.Component {

	render() {
		const header = (
			<div className={theme.header}>
				<div className={theme.list}>
					<div className={theme.icon}>
						<SignIcon />
					</div>
					<div className={theme.row}>
						<div className={theme.label}>
							Количество выгружаемых помещений
						</div>
						<div className={theme.value}>
							<mark>390</mark> объектов
						</div>
					</div>
					<div className={theme.row}>
						<div className={theme.label}>
							Количество выгружаемых помещений
						</div>
						<div className={theme.value}>
							<mark>390</mark> объектов
						</div>
					</div>
				</div>
			</div>
		);

		const footer = (
			<Fragment>
				<Button isPrimary={true}>
					Выгрузить 300 выписок ЕГРН
				</Button>
			</Fragment>
		);
		return (
			<Popup isOpened={true} header={header} footer={footer}>
				<div className="d-flex">

					<Card>
						<CardTitle>Минимальный</CardTitle>
						<CardBody>
							<ul>
								<li>200 помещений</li>
								<li>Кол-во обновлений: <mark>5</mark></li>
								<li><mark>25</mark> руб. за помещение</li>
							</ul>
						</CardBody>
						<CardFooter>
							<Button isExtra={true}>Оплатить</Button>
						</CardFooter>
					</Card>

					<Card>
						<CardTitle>Средний</CardTitle>
						<CardBody>
							<ul>
								<li>Выписки по 100 помещения</li>
								<li>Собрания 1 шт</li>
								<li>Консультации 0 шт</li>
							</ul>
						</CardBody>
						<CardFooter>
							<Button isExtra={true}>Оплатить</Button>
						</CardFooter>
					</Card>

					<Card>
						<CardTitle>Оптимальный</CardTitle>
						<CardBody>
							<ul>
								<li>Выписки по 100 помещения</li>
								<li>Собрания 1 шт</li>
								<li>Консультации 0 шт</li>
							</ul>
						</CardBody>
						<CardFooter>
							<Button isExtra={true}>Оплатить</Button>
						</CardFooter>
					</Card>

				</div>
				<p>
					<a href='#'>Перейти на страницу тарифов</a>
				</p>
			</Popup>
		)
	}
}

export const SelectTariffPopup = withTheme(Symbol(), theme)(RawSelectTariffPopup);
