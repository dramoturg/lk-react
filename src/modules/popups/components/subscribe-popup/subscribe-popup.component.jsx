import React from 'react';
import {Popup} from "../../../ui-kit/components/popup/popup.component";
import {Button} from "../../../ui-kit/components/button/button.component";
import {Alert, Col, Container, Row} from "reactstrap";

import * as theme from './theme/subscribe-popup.component.module.styl';

export class SubscribePopup extends React.Component {

	render() {
		const {isOpened, onRequestClose} = this.props;
		return (
			<Popup theme={theme} isModal={true} isOpened={true} onRequestClose={onRequestClose}>
				<div className={theme.container}>
					<p>
						Мы еженедельно мониторим изменения данных в выписках
						ЕГРН на помещения по всем адресам, которые Вас интересуют.
						Изменения быть, например, при изменении прав собственности на помещение.
					</p>
					<Alert color='secondary'>
						<p>
							Для того, чтобы всегда быть в курсе  изменений подпишитесь на
							обновления данных во всех выгружаемых Вами реестрах.
							Уведомления об имеющихся изменения будут приходить на указанный вами Еmail  и по СМС
						</p>
					</Alert>
					<Container fluid className={theme.list}>
						<Row>
							<Col lg={3} xs={4}>
								<div className={theme.package}>
									<div className={theme.packageTitle}>
										Разовое <br/>
										обновление данных
									</div>
									<div className={theme.packagePrice}>
										<mark>700</mark> руб.
									</div>
									<Button isBlock={true} isPrimary={true}>Подписаться</Button>
								</div>
							</Col>
							<Col lg={3} xs={4}>
								<div className={theme.package}>
									<div className={theme.packageTitle}>
										На 3 месяца <br/>
										обновление данных
									</div>
									<div className={theme.packagePrice}>
										<mark>800</mark> руб.
									</div>
									<Button isBlock={true} isPrimary={true}>Подписаться</Button>
								</div>
							</Col>
							<Col lg={3} xs={4}>
								<div className={theme.package}>
									<div className={theme.packageTitle}>
										На 9 месяцев <br/>
										обновление данных
									</div>
									<div className={theme.packagePrice}>
										<mark>1500</mark> руб.
									</div>
									<Button isBlock={true} isPrimary={true}>Подписаться</Button>
								</div>
							</Col>
							<Col lg={3} xs={4}>
								<div className={theme.package}>
									<div className={theme.packageTitle}>
										На 12 месяцев <br/>
										обновление данных
									</div>
									<div className={theme.packagePrice}>
										<mark>2000</mark> руб.
									</div>
									<Button isBlock={true} isPrimary={true}>Подписаться</Button>
								</div>
							</Col>
						</Row>
					</Container>
				</div>
			</Popup>
		)
	}
}
