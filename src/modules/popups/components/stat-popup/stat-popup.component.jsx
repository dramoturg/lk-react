import React, {Fragment} from 'react';
import {Popup} from "../../../ui-kit/components/popup/popup.component";
import * as theme from './theme/stat.component.module.styl';
import {Alert} from "reactstrap";
import {Button} from "../../../ui-kit/components/button/button.component";

export class StatPopup extends React.Component {

	render() {
		const {isOpened, onRequestClose} = this.props;
		const footer = (
			<Fragment>
				<Button isPrimary={true}>Выгрузить 1150 выписок ЕГРН</Button>
			</Fragment>
		)
		return (
			<Popup theme={theme} footer={footer} isModal={true} isOpened={isOpened} onRequestClose={onRequestClose}>
				<div className={theme.container}>
					<ul className={theme.list}>
						<li>
							<div>Количество адресов по которым необходимо выгрузить данные</div>
							<div><mark>1</mark></div>
						</li>
						<li>
							<div>Количество помещений в выделенных адресах</div>
							<div><mark>1800</mark></div>
						</li>
						<li>
							<div>Количество выгружаемых помещений</div>
							<div><mark>1500</mark> из <mark>1800</mark></div>
						</li>
						<li>
							<div>Количество выгружаемых помещений</div>
							<div><mark>1150</mark></div>
						</li>
					</ul>

					<Alert color='secondary'>
						<p>
							На вашем балансе недостаточно средств для выгрузки всех необходимых помещений. <br/>
							Вы можете выгрузить  <mark>1150</mark> помещений или пополнить баланс,
							чтобы произвести выгрузку  всех необходимых помещений (<mark>1800</mark> <strong>шт.</strong>)
						</p>
					</Alert>
				</div>
			</Popup>
		)
	}
}
