import React, {Fragment} from 'react';
import {withTheme} from '@devexperts/react-kit/dist/utils/withTheme';
import {Popup} from "../../../ui-kit/components/popup/popup.component";
import * as theme from './theme/well-done.popup.component.module.styl';

export class WellDonePopup extends React.Component {

	render() {
		const {isOpened, onRequestClose} = this.props;

		const header = (
			<Fragment>
				<mark>Оплата поступила</mark>
			</Fragment>
		);

		return (
			<Popup theme={theme} isModal={true} isOpened={isOpened} header={header} onRequestClose={onRequestClose}>
				Заказ запущен в работу, это займет некоторое время (от 1 часа до нескольких дней)
				По завершению подгтовки реестра Вы получите email-уведомление <br/>
				Ожидайте уведомления.
			</Popup>
		)
	}
}
