import React from 'react';
import {BalanceInfo} from "./components/balance-info/balance-info.component";
import {ButtonIcon} from "../ui-kit/components/button-icon/button-icon.component";
import {BurgerIcon} from "../ui-kit/components/icons/burger.icon";
import {AccountInfo} from './components/accounf-info/account-info.component';
import {LogoutIcon} from "../ui-kit/components/icons/logout.icon";

import logo from './images/logo.png';
import css from './theme/header.component.module.styl';
import buttonIconTheme from './theme/button-icon.child.module.styl';
import buttonIconLogoutTheme from './theme/button-logout-icon.child.module.styl';


export class Header extends React.Component {
	render() {
		const {onToggleSidebar} = this.props;
		return (
			<div className={`${css.container}`}>
				<ButtonIcon theme={buttonIconTheme} icon={<BurgerIcon/>} onClick={onToggleSidebar}/>
				<div className={css.logo}>
					<img src={logo} alt=""/>
				</div>

				<div className={`${css.balanceInfo} d-md-none d-lg-block`}>
					<BalanceInfo/>
				</div>

				<div className={css.accountInfo}>
					<AccountInfo/>
				</div>
				<ButtonIcon icon={<LogoutIcon/>} theme={buttonIconLogoutTheme}/>
			</div>
		)
	}
}
