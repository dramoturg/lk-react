import React from 'react';
import moveToNextIcon from './icon/move-to-next.png';
import {Button} from "../../../ui-kit/components/button/button.component";
import {Popover} from "../../../ui-kit/components/popover/popover.component";
import {AccountIcon} from "../../../ui-kit/components/icons/account.icon";
import refLinkIcon from '../../icons/ref-link.png';
import css from './theme/account-info.component.module.styl';
import {Link} from "@devexperts/react-kit/dist/components/Link/Link";

import buttonTheme from './theme/button.child.module.styl';
import linkTheme from './theme/link.child.module.styl';

export class AccountInfo extends React.Component {
	anchor;

	state = {
		isOpened: false,
	};

	render() {
		const { isOpened } = this.state;

		return (
			<Button isFlat={true} ref={el => (this.anchor = el)} onClick={this.handleClick} theme={buttonTheme}>
				<div className={`${css.accountIcon} d-none d-sm-block`}>
					<AccountIcon />
				</div>
				<Popover
					isOpened={isOpened}
					closeOnClickAway={true}
					onRequestClose={this.handleRequestClose}
					hasArrow={false}
					anchor={this.anchor}>
					<ul>
						<li>
							<div className={css.refLinkLabel}> Моя реферальная ссылка</div>
							<Link href='#' theme={linkTheme}>
								<img src={refLinkIcon}/>
								собиратьлегко.рф/acc/ivanov
							</Link>
						</li>
					</ul>
					<div className={css.anchorInner}>
						Иванов Евгений Александрович
						<div className={css.toggleIcon}>
							<img src={moveToNextIcon}/>
						</div>
					</div>
				</Popover>
			</Button>
		)
	}

	handleClick = () => {
		this.setState({
			isOpened: !this.state.isOpened,
		});
	};

	handleRequestClose = () => {
		console.log('rt')
		this.setState({
			isOpened: false,
		});
	};
}
