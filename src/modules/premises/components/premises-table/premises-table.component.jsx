import React from 'react';
import {Checkbox} from "../../../ui-kit/components/dx-checkbox/checkbox.component";
import {Location} from "../../../ui-kit/components/location/location.component";
import {Column, RenderTable} from "../../../ui-kit/components/render-table/render-table.component";
import {TableCell} from "@devexperts/react-kit/dist/components/Table/Table";
import {data} from './premises-table.fixture';

import css from './theme/premises-table.component.module.styl';

const cellRenderer = f => cellData => <TableCell>{f(cellData)}</TableCell>;
const mobileRenderer = f => (cellData, dataKey) => <div className={`${css.item} ${css[`item_${dataKey}`]}`}>{f(cellData)}</div>;
const blankRenderer = () => null;
const simpleRenderer = cellData => <div>{cellData}</div>;
const locationRenderer = cellData => {
	return (
		<Location>{cellData}</Location>
	)
};

const isCheckedRenderer = value => <Checkbox value={value}/>;

export class PremisesTable extends React.Component {

	render() {
		return (
			<RenderTable data={data}>
				<Column
					cellRenderer={cellRenderer(isCheckedRenderer)}
					headerRenderer={this.checkHeaderRenderer}
					mobileCellRenderer={mobileRenderer(isCheckedRenderer)}
					dataKey={'isChecked'}
				/>
				<Column dataKey={'index'}
						label={'#'}
						mobileCellRenderer={mobileRenderer(blankRenderer)}
						cellRenderer={cellRenderer(simpleRenderer)}/>
				<Column dataKey={'number'}
						label={'Кадастровый номер'}
						mobileCellRenderer={mobileRenderer(simpleRenderer)}
						cellRenderer={cellRenderer(simpleRenderer)}/>
				<Column dataKey={'address'}
						label={'Адрес'}
						mobileCellRenderer={mobileRenderer(locationRenderer)}
						cellRenderer={cellRenderer(locationRenderer)}/>
				<Column dataKey={'square'}
						label={'Площадь'}
						mobileCellRenderer={mobileRenderer(simpleRenderer)}
						cellRenderer={cellRenderer(simpleRenderer)}/>
				<Column dataKey={'type'}
						label={'Тип помещения'}
						mobileCellRenderer={mobileRenderer(simpleRenderer)}
						cellRenderer={cellRenderer(simpleRenderer)}/>
				<Column dataKey={'floor'}
						label={'Этаж'}
						mobileCellRenderer={mobileRenderer(simpleRenderer)}
						cellRenderer={cellRenderer(simpleRenderer)}/>
				<Column dataKey={'comment'}
						label={'Комментарий'}
						mobileCellRenderer={mobileRenderer(simpleRenderer)}
						cellRenderer={cellRenderer(simpleRenderer)}/>
			</RenderTable>
		);
	}

	checkHeaderRenderer = () => {
		return (
			<div>
				<Checkbox checked={false} />
			</div>
		);
	};
}
