import React from 'react';
import {Button} from "../../../ui-kit/components/button/button.component";

import css from './theme/premises-summary.component.module.styl';
import buttonTheme from './theme/button.child.module.styl';
import {ButtonNavigation} from "../../../ui-kit/components/button-navigation/button-navigation.component";

export class PremisesSummary extends React.Component {
	render() {
		return (
			<div className={css.container}>
				<div className={css.selected}>
					<span className={css.label}>ВЫБРАНО</span>:
					<span className={css.count}> 390 </span> из них
					<span className={css.count}> 370 </span>жилых помещений, <span className={css.count}>20</span> нежилых помещений
				</div>
				<div className={css.buttons}>
					<ButtonNavigation theme={buttonTheme}>
						Сохранить и вернуться
					</ButtonNavigation>
					<ButtonNavigation theme={buttonTheme} isNext={true}>
						Сохранить выборку и оформить заказ
					</ButtonNavigation>
				</div>
			</div>
		);
	}
}
