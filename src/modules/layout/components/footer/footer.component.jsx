import React from 'react';
import css from './theme/footer.component.module.styl';
import {Nav, NavItem, NavLink, Container, Row, Col} from 'reactstrap';
import vkIcon from './support/vk.png';
import viberIcon from './support/viber.png';
import telegaIcon from './support/telega.png';
import fbIcon from './support/fb.png';
import {MailIcon} from "../../../ui-kit/components/icons/envelope.icon";

export class Footer extends React.Component {
	render() {
		return (
			<Container fluid className={`${css.container}`}>
				<Row>
					<Col sm={6} lg={3}>
						<div className={css.logo}>
							<img
								className='img-fluid'
								src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUcAAABACAYAAACJO7VnAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQyIDc5LjE2MDkyNCwgMjAxNy8wNy8xMy0wMTowNjozOSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTggKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkFDRjc4NUJEQjREQjExRTg5OTZFQjU0MTIxN0Q1RDVEIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkFDRjc4NUJFQjREQjExRTg5OTZFQjU0MTIxN0Q1RDVEIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QUNGNzg1QkJCNERCMTFFODk5NkVCNTQxMjE3RDVENUQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QUNGNzg1QkNCNERCMTFFODk5NkVCNTQxMjE3RDVENUQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz52wskRAAAPRElEQVR42uydf4ycRRnH546rKEXKYtUQadS9qsgJUbdCiSZCswcaD4yVOyBIIRpvpf8YIriX2H/82bsI8geJcBsjWEXJLTVpLQZ7S8GAgeItJraHYrwNlRDBwG1pgYKFnvN0n3Hnps+8M++77/vu3u3zTZ7s7vt7Zmc+7zPPzDtvz/79+0WX6hpp10v7orTXBIvF6joNDAxY1/V2aZ5cKW2btLy0XdJO4WLCYrG6HY6XS/ullvaLGZAsFqvb4QhgvE/aCmM5A5LFYnUtHG1gZECyWKyuhSMFxiPS/syAZLFY3QpHGxiHpF0k7SEGJIvF6jY4UmD8L4Jxj2gM4RmyAHKHtLdxEWGxGI7dAMaj0oYRjEo2QOZxfwYki8VwXPZgvELaTmlnS/uKByAvY0CyWAzHbgHjGmkzojEI/KsMSBaL1Q1w3OAAI2iltJO09WsYkCwWaznDEcC4ywFG0N+lfULaJmnrpB1gD5LFYi1XOCowviOgKf2ktNUaIAF010vrkfYzaRsJQD7KgGSxGI7LFYyg/egt6lMQvS7tk/gJgCxbAPkEA5LFYjhGUTsh4QNG0O3SFqT9WFpR2qlGE/t1zAsTkC9Lu0Q0OnAoQJ7ERYjFYjhSukDaP/Gz08CoQ3uLtPdL65c2jtfsC8jTpJ1JnP8paW9xEWKxGI4UGHeLRjxvd8qApMB4TDQmsFUxxpek/UZb/6y0efz+XgKQFyBcoYm9CpefJRodM+8zzj8hbYyLD4vFcLSB8TTNu0oLkDYwbkKvD3Q3gu8q0RjwrXuQP7QA8q/SPi3tOml3IRgfRm+TwchidZl6IrwmwQSjrkOiEaPb2wYw3qMtgyb1Pmkl9CLh92Zt/Q+kfQe/vyBtrbRXtPUMRharCxTnaxKCwJi0B/lZBxhhmE4Ol8PkEh+R9qK0b0q7QdpPDQ/yDs2D3MdgZLFYUeF4rgOMSQISjrXTAcZ/SHtM2qe0bSDm+Ax+NwG5GQEJvdjfZzCyWKwocAQw7vEAYxKApLxVsyl9Nq6Hp2PWS3u74UE+HwDID0r7eQAYb2MwslgMxyAwrrasfw6NAuT9uH+SYATBkyyXSrsRm8kHNQ8SAPkh0YgtUoA8EADGO6V9i4sJi8VwjALGi0Rj7sMXifXvwv2jANIFRrim7VoaHhSNxwBhkPfJ0v6kARI6W9ZqgPyGtA87mtJ3ome5wMWExWI4RgEjDIeBcYIbLIBcHQGQLjD2iUaMEQZrP6ltAxC8ErddYQHkv6TdjPszGFksVig4usAIuhfBqAQ9vjss24YBpK1HXG9KvyntD/gderCHtbT8Fn/bAAlPytwq7d3SHmAwslgsXzj6gBEEsTi9owIey/tawPY+gLSBcbNYHGMEXS3tQvw+hXD2AaRAMMK1DDAYWSyWDxx9wai0FQEJg6qLHtsHATIIjHfgvs+LxR0kj0v7OH4/xwHI9QYYP8ZgZLFYNulPyLjACPHEN8SJzxlHERxrg2gOvnaBEfSItM8gvGD4zQFtO2geX4rfn8K0HMPfG7EpfRuDkcVi6Qp6QkbBEbZ42AFGgNkR3M4HkEfQy/ue5bjqmAp8qwLACILHAOFiYfqxOi671ROQaXuM4LFCD35WNJ7ayUirSauilbQ0tFNzeI1KFWmDXGVi0yj+92E1wVnXGXAE0O0NAJ7p5a31ACSAcUhrQts80oP4eboFjKciNPVxlF9HuIBuCgDkX0RjMts0wQhQHDeAQ6mOFSDtSqBgnUV45431DMd4NU3ksVeLjrOu/XCE+BzE8f4YsP8OsfjZY+ihvjdg+6MaGAXuu0EDoa7THWCEcz0tFr8ECwD4Kn6/RSyOQX5ONHux73GA8dcxgxGgOOUBRoHexDhWnkwKZQCgPYOe4hSem6q0Fa4uLFYTjjBh6yaEBSXogd6i/R4TwU+NQOfH+cay84Tfo4d6UxpesQpPu6wUi3vBYV7GjzoAeaFoDtexgXFTjGAsCr8OKVP5hAEJx51EIOY8vNkSVwkWq6E+/FSAhGeSNxLbwcQMb+L3rR7HhW1eRtDBBLTbhPtpHADVfwyAwQzc0PlyGCvveQhHBci/ITxvwX1UE/txDzDGNYt3Hj0xSnqcMYfbmiCsiuTij5PoNbpUx+Z0natErMpzFixdmfM5wryH8G6Uy2I6/i+kXSv8J7hYwMq8XVsG3uO/MQ7zKkLxWVy3RgPkAsIUHhGEOOWjKYBRCHtcCTzsCcKT071M8NQKnufJotXQXBpFOAoCxhUNhPBZZjAmIqpl0kO0HIK2WU4KW4YTV5j5HGGSBngHy+9CHP8oNmupRwevs4AROmwOWQoFVNQva8sAdjfj95UIwzVGExuO9SMNjLtTAmPOAsaCoDtb6gjNgicYswg4eL3DHFakOfw9KYLjm2Yzv4znLotmx4zLc53GCq4blQcLhE0ToNDXZbG5ry93pUn31KeJYxYJz7xoub4Fx3X7pL0dnugklgF1TTPC3QkYNu0+6afWz8dchtsqClxhAKleaPUTYX+2mtrnS6IxY7gvIKG5fJMByPdogIQKsUUD4/kpgFFYmqwV4Y7d+YBxFAs+NRwkY6yngJU1mvcKKuN43apnfUaDVdSme5RKPkXkX1CaMriPgmDeAs0Z4Y6vLjXpaR81/qsc5seciBb3jiIq/1VoJq4ynPRN5jjQZ2dnp8PA0ReQb4nFr0BVvdLzHjCFHuW9LQISJrtQw4OOtQGMwlIJ4+jUGEboZDwqDRVXzBN374yjsMxEAGSxBRDlHGnKed6MKE8lrVEArv8mztCNT9rVzS9psFDnGMRWSFxluCM9R19AwjubzzGWQefJOwOOeZdY/D7pvQF/Zg/eLb9gASRk6tX4vR1gtFXwagyVKqw3ZhbCbArnzSVcEScJz6QUIi3FNtetuLxX2w2obgmHFEVyHUHKg6XCSNWYy3BHw9EHkOrZahAMvYHOnBWOpuIN2m/Y57uO69tpASS8RfD2NoLR5h20GmimmiAqVjmIn3VLE8UFR3WMQUHPbp4PUaknW0wn5FM/3gRHiDRR8dwJDFsUtHQMCjq+m9f26dGsYrkRKxv0iN3NIyRGI6Q5Chyp5usZaAVLGQpS1ci/oDJheqYZAoylBMpw29XnsY0CpK0Xeyt6kFc5wKikZuF+zgOmOiABpPdrgAzyGO9LAYxpehwF0XztbAUr2VRIT8XsPa9oBd6EStWjwuZigKOCRdnibeQMmNWwcqlez5zmUZowTTLumBHNuO2wBe5xwJEa+jVm5EkJ86IYIvxQF9EG/I8SN6tSimVYHxkygt9HjbyYsORzRiu3ejnJz87OzmGZLw8MDJR9PUdfD/JaC+QgxnjYAsjtnjBV17nDSJQNjHCN16QExloCzak8cY6ysaxMnNvVlCp7Lst7gKFIXGOrT9f4XEtONHs7pxHsqve2XWMK8xYvOo5e2JzFYzd7iYsJNuuDVEq5DOuiPHfVwZMj8mIuIOSQxRvKlATlZFg46oDc5bm96nyBO/2hEF7rr0RjqI8piHE+gIkLAuMVeK1pqBoBLmGb6jVPMGcc+/guc6WXajJNxACDugcopkVnDqweJipkJkIakw75xK3JBMtwK+meNMAXpoNuVAJyPCwcFSDhNQQPeYIRmsNBvdKCqGTghQ45APlIB4DRBkefmVjUH5hpoZBnQnq0Wc9ltZBeY0Vr2rUi17VMEWkuaeGCNMCj4pJjnl6uT3lJSmk8J58Xfh1fUcqwj1TsuWD8/zkttGCOfVVxTz2f+o3/pigBme2NcEGvIbyCABmmV1oHoyp0exyAPLcDwGhrCmYdac2I5ng16o5WISpZljhHLmA/qmKMey6rOQBGxcDi8r5s10LlwYRoDrYfSxk8rtmUcpb0hL3GquXcPYSdYfxOK91F4r+Jowz7NuvVzblguTmNGsc/IQ0DAwM1ohwP90bMJBcgw/ZKg/YZv+FFWAc9rqVdYFSVt2RJ/4xRQVTQfE7743LGb1uF0L0m9WRJkKdAPfUyjPupqcqmLBW4HCL9rUAph/mkD063XUvG4rVkAipfkio60jVpKSthK3+F+B+LhjeUw/8yjcHvY0T5oDrS4ijDrToq1GgH6/ElIM11+b4WLkYBEmKQFxPrVa/005gRrs6XbfgJU42pOSPPdOzzTBvBqBeYYaIC5ywFgPIkc1qBKhGVD9bPi+YEFlQsq0zc5ccJQA477sS1EBW3lfkoXWPh9GupWiA0n/J/veDIj6CBz1EfDqD+R9vNBFoiIwk3qQvixF501WtcirkMhy1PLoUKvfS2mFEuDxIA+XvReI+0z7UAIL8t/GYbfwGb1+0Eo8rwVma0MceJ1QKgkwuoQDViWRivrhaiiVwX/hNmRFHVuJa66OzZsSuiOaFH3DeSiZCwq6dQ3icsIZpMzGU4jEaJPK95nlvMzs6a62q9MWSWC5AnB3iKhwhATniCEbzLVzqkcqhBtbWQMFpn8SjGQngaQTOKD3pWrCpei2/FKojWB7vXA0BC3WxcsJ9o438/EgCOsrY+qkY8yoO6SacRe6WAbbYEWinDamIRZTaNo4c6SXjSCo41A6BFAoxZohVT6Y0ps3w6aczMgBl7fHuxdUGM8QMdBEYTMGMOcKgnBdY5CnIBK0XNAZExjwozSDSZ61rFDQPGUovNHz2/+vH6x7Q8sXnhdVxfMCpmWdifukhKatxegcg7BXF1XSMxXJfy1Kn/UXnZ/SLdTimq3Jlhm6hlWMULqVd56CoiFEeJG1LVcp3jBnDh+CfE/WEwuDmfY6s6RdhjkHrh0S/Y9uZBGxjbHWP0leokyBoFOUo8SH/3S1V00Hx4IbRgqRis7lCYMmyOZ+2xLLcB17wh2eY2tbYCJRzrfTFngKuT5rgXa/x+Cb1AFxyXEhhVJsd1J1+KMGSxki7Dg6I5H4Cadb9kae3UxOK3glJQrUgo/r95H7fnqHuQD0pbT6yD6cXguWfolT5L2mP4GSSY4uzyJQRGFnuOrPhl8yjDSjXHj0sCkTxOb0KJAA8SXnT1hOWc0Blzo2j0SrvACHHMjQxGFouVpnoTPDa8YOuSAEDC7OH9HmAcQtiyWCxWaupL+PgKkNQkES4xGJeX+EX1rKVVYBOKOZpaFRKQDEYWi5W4wrx9sB1NbAYji8XqOPWmeC4fQDIYWSxW18HRBUgGI4vF6lo42gDJYGSxWB2lvjadVwESBgIfZjCyWCyG42JAfl7aGwxGFovVafqfAAMA/N/KBJN3v2MAAAAASUVORK5CYII="
								alt=""
							/>
						</div>
						<div className={css.customerSupport}>
							Клиентская поддержка
							<div className={css.supports}>
								<a href="#" className={css.support}> <img src={telegaIcon} /></a>
								<a href="#" className={css.support}><img src={vkIcon} /></a>
								<a href="#" className={css.support}> <img src={fbIcon} /></a>
								<a href="#" className={css.support}> <img src={viberIcon} /></a>
							</div>
						</div>
					</Col>
					<Col lg={3} className="d-none d-lg-block">
						<Nav>
							<NavItem><a href='/'> О нас</a></NavItem>
							<NavItem><a href='/'>Официальные документы</a></NavItem>
							<NavItem><a href='/'>ЧАВО</a></NavItem>
							<NavItem><a href='/'>Контакты</a></NavItem>
						</Nav>
					</Col>
					<Col lg={3} className="d-none d-lg-block">
						<Nav>
							<NavItem><a href='/'>Регистрация</a></NavItem>
							<NavItem><a href='/'>Вход в личный кабинет</a></NavItem>
							<NavItem><a href='/'>Вход для специалистов со своим ключом</a></NavItem>
						</Nav>
					</Col>
					<Col sm={6} lg={3}>
						<div className={css.phone}> 8-800-800-80-80</div>
						<div className={css.timeOpen}>
							Офис работает <br/>
							с Пн по Пт с 10:00 до 09:00
						</div>
						<div className={css.supportLink}>
							<div className={css.supportLinkIcon}>
								<MailIcon />
							</div>
							<a href="#"> support@namedomain.ru</a>
						</div>
					</Col>
				</Row>
				<Row className="d-none d-lg-block d-xl-none">
					<Col xs={12}>
						<div className={css.copyright}>
							с 2018 «Название сервиса» Все права защищены
						</div>
					</Col>
				</Row>
			</Container>
		)
	}
}
