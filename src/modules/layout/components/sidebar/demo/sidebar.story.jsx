import React from "react";
import { storiesOf } from "@storybook/react";
import { Theme } from '../../../../ui-kit/components/theme/theme.component'
import { Sidebar } from "../sidebar.component";


storiesOf("Sidebar", module)
	.add("default", () => (
		<Theme>
			<Sidebar />
		</Theme>
	))