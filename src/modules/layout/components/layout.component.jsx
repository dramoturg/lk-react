import React from 'react';
import {Sidebar} from "./sidebar/sidebar.component";
import {Footer} from "./footer/footer.component";
import {Header} from "../../header/header";
import { withTheme } from '@devexperts/react-kit/dist/utils/withTheme';
import * as css from './theme/layout.component.module.styl';

class RawLayout extends React.Component {

	state = {
		isOpenedSidebar: false
	}

	render() {
		const {isOpenedSidebar} = this.state;
		const {children} = this.props;

		return (
			<div className={css.container}>
				<Header onToggleSidebar={this.onToggle}/>
				<div className={css.wrapper}>
					<Sidebar isOpenedSidebar={isOpenedSidebar} onToggle={this.onToggle}/>
					<div className={css.content}>
						{children}
					</div>
				</div>
				<Footer />
			</div>
		)
	}

	onToggle = () => {
		this.setState({
			isOpenedSidebar: !this.state.isOpenedSidebar
		})
	}
}

const theme = {
	...css,
};

export const Layout = withTheme(Symbol(), theme)(RawLayout);
