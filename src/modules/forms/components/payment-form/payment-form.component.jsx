import React from 'react';
import {Col, Form, FormGroup, Label } from 'reactstrap';
import {Selectbox} from "../../../ui-kit/components/selectbox/selectbox.component";
import {Badge} from "../../../ui-kit/components/badge/badge.component";
import {Input} from "../../../ui-kit/components/input/input.component";
import {MenuItem} from "../../../ui-kit/components/menu/menu.component";

import selectboxTheme from './theme/selectbox.child.module.styl';

export class PaymentForm extends React.Component {
	render() {
		return (
			<Form>
				<FormGroup row className="align-items-center">
					<Label lg={1} sm={2}>Плательщик</Label>
					<Col sm={10} lg={11}>
						<Selectbox
							placeholder="Controlled by left"
							theme={selectboxTheme}
							value={'superman'}
							onValueChange={() => console.log('t')}>
							<MenuItem value="superman">ООО «ЖКС1 Приморского района»</MenuItem>
							<MenuItem value="batman">Batman</MenuItem>
							<MenuItem value="flash">Flash</MenuItem>
						</Selectbox>
					</Col>
				</FormGroup>
				<FormGroup row className="align-items-center">
					<Label lg={1} sm={2}>Тариф</Label>
					<Col sm={10} lg={11} className="d-flex align-items-center">
						<Selectbox
							placeholder="Controlled by left"
							theme={selectboxTheme}
							value={'superman'}
							onValueChange={() => console.log('t')}>
							<MenuItem value="superman">1000 помещений за 15 000 руб</MenuItem>
							<MenuItem value="batman">Batman</MenuItem>
							<MenuItem value="flash">Flash</MenuItem>
						</Selectbox>
						<Badge>
							1.5р за помещение
						</Badge>
					</Col>
				</FormGroup>
				<FormGroup row className="align-items-center">
					<Label lg={1} sm={2}/>
					<Col sm={10} lg={11} className="d-flex align-items-center">
						<Input value={'15 000'}/>
						<Badge>
							1000 помещений
						</Badge>
					</Col>
				</FormGroup>
			</Form>
		);
	}
}
