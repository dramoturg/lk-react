import React from 'react';
import {Form, FormGroup } from 'reactstrap';
import {Input} from "../../../ui-kit/components/input/input.component";
import {Button} from "../../../ui-kit/components/button/button.component";
import {SearchIcon} from "../../../ui-kit/components/icons/search.icon";

import inputTheme from './theme/input.child.module.styl';
import buttonTheme from './theme/button.child.module.styl';
import css from './theme/search-form.module.styl';

export class SearchForm extends React.Component {
	render() {
		return (
			<Form>
				<FormGroup className={`${css.container} d-flex align-items-center`}>
					<div className={css.searchIcon}>
						<SearchIcon />
					</div>
					<Input theme={inputTheme} placeholder="Введите адрес в соответствии с форматов КЛАДР" />
					<Button theme={buttonTheme} isPrimary={true}>Загрузить</Button>
					<a href="#">Пример реестра</a>
				</FormGroup>
				<FormGroup>
					<p>
						<a href="#">Массовая загрузка домой</a>
					</p>
				</FormGroup>
			</Form>
		);
	}
}
