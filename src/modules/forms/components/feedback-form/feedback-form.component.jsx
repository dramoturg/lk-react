import React from 'react';
import {Form, FormGroup} from 'reactstrap';
import {Input as BInput} from 'reactstrap';
import {Input} from "../../../ui-kit/components/input/input.component";

import * as theme from './theme/feedback-form.component.module.styl';

export class FeedackForm extends React.Component {
	render() {
		return (
			<Form>
				<FormGroup>
					<Input placeholder='Ваше имя'/>
				</FormGroup>
				<FormGroup row>
					<Input placeholder='Номер телефона'/>
				</FormGroup>
				<FormGroup>
					<BInput type="textarea" placeholder='Ваш вопрос'/>
				</FormGroup>
			</Form>
		);
	}
}
