import React from 'react';
import {FormControl} from "../../ui-kit/components/form-control/form-control.component";
import {Input} from "../../ui-kit/components/input/input.component";
import {Selectbox} from "../../ui-kit/components/selectbox/selectbox.component";
import {MenuItem} from "@devexperts/react-kit/dist/components/Menu/Menu";
import {Button} from "../../ui-kit/components/button/button.component";

import css from './theme/registries-filter.component.module.styl';
import formControlTheme from './theme/form-control.child.module.styl';
import selectboxTheme from './theme/selectbox.child.module.styl';
import selectboxCountOnPageTheme from './theme/selectbox-count-on-page.child.module.styl';

import {Col, Container, Row} from "reactstrap";

export class RegistriesFilter extends React.Component {

	render() {
		return (
			<div className={css.container}>
				<div className={css.controls}>
					<FormControl title="Кадастровый номер" theme={formControlTheme}>
						<Input />
					</FormControl>
					<FormControl title="Тип объекта" theme={formControlTheme}>
						<Selectbox
							placeholder=""
							value={1}
							theme={selectboxTheme}
							onValueChange={() => console.log('t')}>
							<MenuItem value={1}>
								Жилые помещения
							</MenuItem>
							<MenuItem value={2}>
								Нежелые помещения
							</MenuItem>
							<MenuItem value={3}>
								Лестничные клетки
							</MenuItem>
						</Selectbox>
					</FormControl>
					<FormControl title="Этаж" theme={formControlTheme}>
						<Selectbox
							placeholder=""
							value={1}
							theme={selectboxTheme}
							onValueChange={() => console.log('t')}>
							<MenuItem value={1}>
								Любой
							</MenuItem>
							<MenuItem value={2}>
								Чердак
							</MenuItem>
						</Selectbox>
					</FormControl>
					<FormControl title="Площадь" theme={formControlTheme}>
						<div className={css.range}>
							<div className={css.rangeInput}>
								<Input />
							</div>
							<div className={css.rangeDelimeter} />
							<div className={css.rangeInput}>
								<Input />
							</div>
						</div>
					</FormControl>
					<FormControl title="&nbsp;" theme={formControlTheme}>
						<Button isPrimary={true}>Показать все</Button>
					</FormControl>
				</div>
				<Row>
					<Col xs={6}>
						Всего выбрано объектов: <strong>390 из 390</strong>,
						из них жилых <strong>370, нежилых 20</strong>
					</Col>
					<Col xs={6}>
						<FormControl title="Показать по" theme={{
							container: css.pager,
							title: css.pagerTitle
						}}>
							<Selectbox
								placeholder=""
								value={1}
								theme={selectboxCountOnPageTheme}
								onValueChange={() => console.log('t')}>
								<MenuItem value={1}>
									100
								</MenuItem>
								<MenuItem value={2}>
									200
								</MenuItem>
							</Selectbox>
						</FormControl>
					</Col>
				</Row>
			</div>
		);
	}
}
