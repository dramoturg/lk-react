import React, {Fragment} from 'react';
import css from './theme/room-table.component.module.styl';
import {Checkbox} from "../../../ui-kit/components/dx-checkbox/checkbox.component";
import {Date} from "../../../ui-kit/components/date/date.component";
import {Location} from "../../../ui-kit/components/location/location.component";
import {RoomFiler} from "../../../ui-kit/components/room-filter/room-filter.component";
import {Status, StatusType} from "../../../ui-kit/components/status/status.component";
import {Col, Container, Row} from "reactstrap";
import { Table, TableCell as TCell, TableHead as THead, TableBody as TBody, TableRow as TRow } from "../../../ui-kit/components/table/table.component";
import {TableFooter} from "../../../ui-kit/components/table-footer/table-footer.component";
import {QuotationIcon} from "../../../ui-kit/components/icons/quotation.icon";
import {Button} from "../../../ui-kit/components/button/button.component";

export class RoomTable extends React.Component {

	render() {
		const {data = []} = this.props;

		return (
			<Fragment>
				<div className='d-none d-xl-block'>
					<Table>
						<THead>
							<TRow>
								<TCell><Checkbox /></TCell>
								<TCell>#</TCell>
								<TCell>Дата выгрузки</TCell>
								<TCell>Адрес</TCell>
								<TCell>Кол-во помещений</TCell>
								<TCell>Статус</TCell>
								<TCell>Комментарий</TCell>
							</TRow>
						</THead>
						<TBody>
							{data.map((cell, index) => {
								const {status} = cell;
								const id = index + 1;
								return (
									<TRow key={`room-table-xl${index}`}>
										<TCell><Checkbox /></TCell>
										<TCell>{id}</TCell>
										<TCell>
											<Date>10.05.2018</Date>
										</TCell>
										<TCell>
											<Location>
												г. Санкт-Петербург, Московское ш.,
												д. 30, корп. 2, лит. А, кв. 1
											</Location>
										</TCell>
										<TCell>
											<RoomFiler selected={100} total={352} />
										</TCell>
										<TCell>
											<Status type={status.type}>
												{status.text}
											</Status>
										</TCell>
										<TCell>
											<div className={css.comment}>
												<div className={`d-block d-sm-none ${css.commentIcon}`}>
													<QuotationIcon />
												</div>
												Скорее всего нет данных
											</div>
										</TCell>
									</TRow>
								)
							})}
						</TBody>
					</Table>
				</div>
				{data.map((cell, index) => {
					const {status} = cell;
					return (
						<div key={`room-table-xs${index}`} className={`${css.item} d-xl-none`}>
							<div className={css.checkboxContainer}>
								<Checkbox />
							</div>
							<div className={css.body}>
								<Container fluid>
									<Row>
										<Col xs={12}>
											<Location>
												г. Санкт-Петербург, Московское ш., д. # 30, корп. 2, лит. А, кв. 1
											</Location>
										</Col>
									</Row>
									<Row className="align-items-center">
										<Col sm={6}>
											<RoomFiler selected={100} total={352} />
										</Col>
										<Col sm={6}>
											<Status type={status.type}>
												{status.text}
											</Status>
										</Col>
									</Row>
									<Row>
										<Col xs={12}>
											Скорее всего данных нет
										</Col>
									</Row>
								</Container>
							</div>
						</div>
					)
				})}
				<TableFooter />
				<Row>
					<Col sm={6} className="text-left">
						<Button isExtra={true}>
							Назад
						</Button>
					</Col>
					<Col sm={6} className="text-right">
						<Button isPrimary={true}>
							Перейти к выбору тарифа и оплате
						</Button>
					</Col>
				</Row>
			</Fragment>
		);
	}
}
