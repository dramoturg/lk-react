import React from 'react';
import { memoize } from '@devexperts/utils/dist/function/memoize';
import {Page} from "../../../ui-kit/components/page/page.component";
import {Tabs} from "../../../ui-kit/components/tabs/tabs.component";
import {Tab} from "../../../ui-kit/components/tabs/tab.component";
import {Section} from "../../../ui-kit/components/section/section.component";
import { Container, Row, Col } from 'reactstrap';
import { YMaps, Map, Placemark } from 'react-yandex-maps';
import {BuildingInfo} from "../../../ui-kit/components/bulding-info/building-info.component";
import {Popup} from "../../../ui-kit/components/popup/popup.component";

export class Step4Taraskin extends React.Component {
	render() {
		const activeTab = 1;
		const mapState = { center: [55.76, 37.64], zoom: 10, controls: [] };

		const header = (
			<div>
				г. Санкт-Петербург, Московское ш. 30, к. 2, лит. А, м
			</div>
		);

		return (
			<Page title="Карточка дома/выбор помещений">
				<Popup isOpened={false} isModal={true} header={<div>Оплата поступила</div>}>
					<p>
						Заказ запущен в работу, это займет некоторое время (от 1 часа до нескольких дней) По завершению
						подготовки реестра Вы получите емаил-уведомление
						Ожидайте уведомления
					</p>
				</Popup>
				<Tabs>
					<Tab isActive={activeTab === 1} handleClick={this.handleTabClick(1)}>
						Общая информация
					</Tab>
					<Tab isActive={activeTab === 2} handleClick={this.handleTabClick(2)}>
						Реестры
					</Tab>
					<Tab isActive={activeTab === 3} handleClick={this.handleTabClick(3)}>
						собрания
					</Tab>
				</Tabs>
				<Container fluid={true}>
					<Row>
						<Col lg={8}>
							<Section>
								<BuildingInfo header={header}>
									<p>
										Владельцы заведений получили предписание освободить участки Фото: pixabay.com В Курортном районе
										Санкт-Петербурга четыре ресторана в скором времени будут снесены. Такое решение принял Смольный,
										разорвав договоры аренды с предпринимателя.
									</p>
									<p>
										Реформы ЖКХ: <br/>
										<a href="https://www.reformagkh.ru/myhouse/profile/view/8197493">https://www.reformagkh.ru/myhouse/profile/view/8197493</a>
									</p>
									<p>
										Сайт ГИС ЖКХ: <br/>
										<a href="#">https://dom.gosuslugi.ru/#!/house-view?guid=18d9c17b-6c36-49ed-80e1-78823b15d90e&typeCode=1</a>
									</p>
								</BuildingInfo>
							</Section>
						</Col>

						<Col lg={4}>
							<Section header={"Местоположение на карте"}>
								<br/>
								<YMaps>
									<Map state={mapState} width="100%" height="215px" options={{suppressMapOpenBlock: true}}>
										<Placemark
											geometry={{
												coordinates: [55.751574, 37.573856]
											}}
											properties={{
												hintContent: 'Собственный значок метки',
												balloonContent: 'Это красивая метка'
											}}
											options={{}}
										/>

									</Map>
								</YMaps>
							</Section>
						</Col>
					</Row>
				</Container>
			</Page>
		);
	}

	handleTabClick = memoize((index) => () => {
		this.setState({
			activeTab: index,
		});
	});
}
