import React from 'react';
import { memoize } from '@devexperts/utils/dist/function/memoize';
import {Page} from "../../../ui-kit/components/page/page.component";
import {Col, Container, Row} from "reactstrap";
import {CustomBuilderCard} from "../../../cards/component/custom-builder-card/custom-builder-card.component";
import {EventPack} from "../../../cards/component/event-pack/event-pack.component";

export class Step2Taraskin extends React.Component {
	render() {
		return (
			<Page title="Покупка действий">
				<Container fluid>
					<Row>
						<Col xs={12} className="d-flex flex-wrap">
							<p>
								Вы можете приобрести любой набор действий в системе.
								Обратите внимание, чем больше действий вы покупаете,
								тем больше скидку вы получаете
							</p>
						</Col>
					</Row>
					<Row>
						<Col xs={12} className="d-flex flex-wrap">
							<CustomBuilderCard header={(<div>Баланс 9000р</div>)} />
							<EventPack price={5000} discount={15} />
							<EventPack price={10000} discount={15} />
							<EventPack price={20000} discount={15} />
							<EventPack price={50000} discount={15} />
						</Col>
					</Row>
				</Container>
			</Page>
		);
	}

	handleTabClick = memoize((index) => () => {
		this.setState({
			activeTab: index,
		});
	});
}
