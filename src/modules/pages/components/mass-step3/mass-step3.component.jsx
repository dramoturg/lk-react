import React from 'react';
import {Page} from "../../../ui-kit/components/page/page.component";
import {Col, Container, Row} from 'reactstrap';
import {BuildingMetrics} from "../../../building-metric/building-metric.component";
import {data as metrics} from "../../../building-metric/building-metric.fixture";
import {Checkbox} from "../../../ui-kit/components/dx-checkbox/checkbox.component";
import {PremisesFilter} from "../../../premises/components/premises-filter/premises-filter.component";
import {PremisesTable} from "../../../premises/components/premises-table/premises-table.component";
import {PremisesSummary} from "../../../premises/components/premises-summary/premises-summary.component";

export class MassStep3 extends React.Component {
	render() {
		return (
			<Page withSteps={true} step={2} title="Мастер массовой подготовки реестра">
				<Container fluid>
					<Row>
						<Col xs={12}>
							<BuildingMetrics metrics={metrics} />
							<p style={{
								margin: '20px 0'
							}}>
								Выбор помещений для заказа по адресу г. Санкт-Петербург, Московское ш., д. № 30, корп. 2, лит. А

							</p>
							<PremisesTable />
							<PremisesFilter />
						</Col>
					</Row>
				</Container>
				<PremisesSummary />
			</Page>
		);
	}
}
