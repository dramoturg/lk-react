import React from 'react';
import { memoize } from '@devexperts/utils/dist/function/memoize';
import {Page} from "../../../ui-kit/components/page/page.component";
import {Tariff} from "../../../cards/component/tariff-card/tariff-card.component";
import {Col, Container, Row} from "reactstrap";
import {Button} from "../../../ui-kit/components/button/button.component";
import {CustomForm} from "../../../custom-form/custom-form.component";
import {PaymentMethod, AvailableType} from "../../../payment-method/payment-method.component";
import {SearchForm} from "../../../forms/components/search-form/search-form.component";

export class Step1 extends React.Component {
	render() {
		return (
			<Page withSteps={true} step={2} title="Мастер подготовки реестра">
				<Container fluid>
					<Row>
						<Col xs={12}>
							<p>
								Для корректной работы сервиса адресс должен соответствовать стандарту КЛАДР
							</p>
						</Col>
					</Row>
					<Row>
						<Col xs={12}>
							<SearchForm />
						</Col>
					</Row>
				</Container>
			</Page>
		);
	}
}
