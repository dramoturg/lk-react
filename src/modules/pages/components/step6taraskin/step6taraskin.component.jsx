import React from 'react';
import {memoize} from '@devexperts/utils/dist/function/memoize';
import {Page} from "../../../ui-kit/components/page/page.component";
import {Tabs} from "../../../ui-kit/components/tabs/tabs.component";
import {Tab} from "../../../ui-kit/components/tabs/tab.component";
import {Section} from "../../../ui-kit/components/section/section.component";
import {Container, Row, Col} from 'reactstrap';
import {YMaps, Map, Placemark} from 'react-yandex-maps';
import {BuildingInfo} from "../../../ui-kit/components/bulding-info/building-info.component";
import {Popup} from "../../../ui-kit/components/popup/popup.component";
import {SummaryCard} from "../../../cards/component/summary-card/summary-card.component";
import {ServiceCard, ServiceCardType} from "../../../cards/component/service-card/service-card.component";
import {DocType, LinkDoc} from "../../../ui-kit/components/link-doc/link-doc.component";
import {ConfirmPopup} from "../../../popups/components/confirm-popup/confirm-popup.component";
import {PremisesTable} from "../../../premises/components/premises-table/premises-table.component";
import {RegistriesTable} from "../../../registries/registries-table/registries-table.component";
import {RegistriesFilter} from "../../../registries/registries-filter/registries-filter.component";

export class Step6Taraskin extends React.Component {
	state = {
		activeTab: 2
	}
	render() {
		const {activeTab} = this.state;

		const mapState = {center: [55.76, 37.64], zoom: 10, controls: []};

		const header = (
			<div>
				г. Санкт-Петербург, Московское ш. 30, к. 2, лит. А, м
			</div>
		);

		return (
			<Page title="Карточка дома/выбор помещений">
				<ConfirmPopup isOpened={false} />
				<Tabs>
					<Tab isActive={activeTab === 1} handleClick={this.handleTabClick(1)}>
						Общая информация
					</Tab>
					<Tab isActive={activeTab === 2} handleClick={this.handleTabClick(2)}>
						Реестры
					</Tab>
					<Tab isActive={activeTab === 3} handleClick={this.handleTabClick(3)}>
						собрания
					</Tab>
				</Tabs>
				{activeTab == 1 && (
					<Container fluid={true}>
						<Row>
							<Col lg={8}>
								<Section>
									<BuildingInfo header={header}>
										<p>
											Владельцы заведений получили предписание освободить участки Фото: pixabay.com В
											Курортном районе
											Санкт-Петербурга четыре ресторана в скором времени будут снесены. Такое решение
											принял Смольный,
											разорвав договоры аренды с предпринимателя.
										</p>
										<p>
											Реформы ЖКХ: <br/>
											<a href="https://www.reformagkh.ru/myhouse/profile/view/8197493">https://www.reformagkh.ru/myhouse/profile/view/8197493</a>
										</p>
										<p>
											Сайт ГИС ЖКХ: <br/>
											<a href="#">https://dom.gosuslugi.ru/#!/house-view?guid=18d9c17b-6c36-49ed-80e1-78823b15d90e&typeCode=1</a>
										</p>
									</BuildingInfo>
								</Section>

								<br/>

								<Section header="Образцы документов">
									<LinkDoc type={DocType.Doc}>
										Скачать Уведомление о проведении собрания собственников жилья
									</LinkDoc>
									<LinkDoc type={DocType.Xls}>
										Скачать Бюллетень голосования (решение) собственников жилья на общем собрании
										собственников МКД
									</LinkDoc>
									<LinkDoc type={DocType.Pdf}>
										Скачать реестр участников очного собрания
									</LinkDoc>
									<LinkDoc type={DocType.Png}>
										Скачать реестр участников очного собрания
									</LinkDoc>
									<LinkDoc type={DocType.Doc}>
										Уведомления администрации (сопроводительные письма)
									</LinkDoc>
									<LinkDoc type={DocType.Xls}>
										Уведомления в управляющую компанию (сопроводительные письма)
									</LinkDoc>
									<LinkDoc type={DocType.Pdf}>
										Доверенность на проведение собрания собственников
									</LinkDoc>
									<LinkDoc type={DocType.Png}>
										Договор на проведение собрания собственников жилья
									</LinkDoc>
									<LinkDoc type={DocType.Doc}>
										Акты о размещении сообщений о проведении внеочередного общего собрания
										собственников помещений в форме очно-заочного голосования в МКД
									</LinkDoc>
								</Section>

								<br/>

								<Section header="Документация">
									<LinkDoc type={DocType.Pdf}>
										Скачать Ст. ЖК РФ № 44 № 45 № 46 № 47 № 48 Обновление 12.06.2018 г.
									</LinkDoc>
									<LinkDoc type={DocType.Png}>
										Скачать Приказ № 937/пр Об утверждении Требований к оформлению протоколов и
										Порядка передачи решений и протоколов общих собраний собственников помещений в МКД в
										органы исполнительной власти с обновлениями от 09 апреля 2018 г.
									</LinkDoc>
									<LinkDoc type={DocType.Doc}>
										Скачать Приказ № 938/пр Об утверждении Порядка и сроков внесения изменений в
										реестр лицензий субъекта РФ с обновлениями от 09 апреля 2018 г.
									</LinkDoc>
								</Section>
							</Col>

							<Col lg={4}>
								<SummaryCard unloaded={370} total={390}>
									<div className="d-flex flex-wrap">
										<ServiceCard type={ServiceCardType.Consult}>
											Получить консультацию по процедуре
											проведения собрания
										</ServiceCard>
										<ServiceCard type={ServiceCardType.Zakaz}>
											Заказать собраниея собствеников жилья МКД
											<small>
												только в санкт-петербурге и
												ленинградской области
											</small>
										</ServiceCard>
										<ServiceCard type={ServiceCardType.Self}>
											Провести собрание самостоятельно
										</ServiceCard>
									</div>
								</SummaryCard>

								<Section header={"Местоположение на карте"}>
									<br/>
									<YMaps>
										<Map state={mapState} width="100%" height="215px"
										     options={{suppressMapOpenBlock: true}}>
											<Placemark
												geometry={{
													coordinates: [55.751574, 37.573856]
												}}
												properties={{
													hintContent: 'Собственный значок метки',
													balloonContent: 'Это красивая метка'
												}}
												options={{}}
											/>

										</Map>
									</YMaps>
								</Section>
							</Col>
						</Row>
					</Container>
				)}

				{activeTab === 2 && (
					<Container fluid={true}>
						<Row>
							<Col xs={12}>
								<RegistriesFilter />
								<RegistriesTable />
								<p style={{
									margin: '20px 0'
								}}>
									<a href="#">Скачать таблицу Excel</a>
								</p>
							</Col>
						</Row>
					</Container>
				)}
			</Page>
		);
	}

	handleTabClick = memoize((index) => () => {
		this.setState({
			activeTab: index,
		});
	});
}
