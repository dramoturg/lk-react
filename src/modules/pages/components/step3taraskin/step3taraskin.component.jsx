import React from 'react';
import {Page} from "../../../ui-kit/components/page/page.component";
import {Col, Container, Row} from "reactstrap";
import {Button} from "../../../ui-kit/components/button/button.component";
import {SearchForm} from "../../../forms/components/search-form/search-form.component";
import {StatPopup} from "../../../popups/components/stat-popup/stat-popup.component";
import {ObjectsTable} from "../../../objects/objects-table/objects-table.component";
import {ObjectsFilter} from "../../../objects/objects-filter/objects-filter.component";

export class Step3Taraskin extends React.Component {
	state = {
		isOpened: true
	}
	render() {
		const {isOpened} = this.state;

		return (
			<Page title="Все добавленные дома ">
				<StatPopup isOpened={isOpened} onRequestClose={this.onRequestClose}/>
				<Container fluid>
					<Row>
						<Col xs={12}>
							<p>
								Для корректной работы сервиса адрес должен соответствовать стандарту КЛАДР
							</p>
						</Col>
						<Col xs={12}>
							<SearchForm />
						</Col>
						<Col xs={12} className="text-right">
							<div style={{
								margin: '20px 0'
							}}>
								<Button isExtra={true}>Подписаться на обновления</Button>
								&nbsp;&nbsp;&nbsp;
								<Button isPrimary={true}>Обновить</Button>
							</div>
						</Col>
						<Col xs={12}>
							<ObjectsTable isVisibleSchedules={true} />
							<ObjectsFilter />
						</Col>
						<Col xs={12} className="text-right">
							<br/>
							<Button isPrimary={true}>Оплатить выделенные</Button>
						</Col>
					</Row>
				</Container>
			</Page>
		);
	}

	onRequestClose = () => {
		this.setState({
			isOpened: false
		})
	}
}
