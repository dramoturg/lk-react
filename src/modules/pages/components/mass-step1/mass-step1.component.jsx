import React from 'react';
import {Page} from "../../../ui-kit/components/page/page.component";
import {Col, Container, Row} from 'reactstrap';
import {MassPreparation} from "../../../mass-preparation/components/mass-preparation.component";
import {Button} from "../../../ui-kit/components/button/button.component";

export class MassStep1 extends React.Component {
	render() {
		return (
			<Page withSteps={true} step={0} title="Мастер массовой подготовки реестра">
				<Container fluid>
					<Row>
						<Col xs={12}>
							<p>
								Для корректной работы сервиса все адреса должны
								соответствовать стандарту КЛАДР.
							</p>
						</Col>
					</Row>
					<Row>
						<Col xs={12}>
							<MassPreparation />
						</Col>
					</Row>
					<Row>
						<Col xs={12}>
							<Button isPrimary={true}>Загрузить</Button>
						</Col>
					</Row>
				</Container>
			</Page>
		);
	}
}
