import React from 'react';
import {Page} from "../../../ui-kit/components/page/page.component";
import {ObjectsTable} from "../../../objects/objects-table/objects-table.component";
import {TableFooter} from "../../../ui-kit/components/table-footer/table-footer.component";
import {Col, Container, Row} from "reactstrap";
import {ButtonNavigation} from "../../../ui-kit/components/button-navigation/button-navigation.component";

export class Step2 extends React.Component {
	render() {
		return (
			<Page step={3} withSteps={true} title={'Мастер подготовки реестра'}>
				<Container fluid>
					<Row>
						<Col xs={12}>
							<ObjectsTable />
							<TableFooter/>
						</Col>
					</Row>
					<Row>
						<Col xs={6}>
							<ButtonNavigation isExtra={true}>Назад</ButtonNavigation>
						</Col>
						<Col xs={6} className="text-right">
							<ButtonNavigation isPrimary={true} isNext={true}>
								Перейти к выбору тарифа и оплату
							</ButtonNavigation>
						</Col>
					</Row>
				</Container>
			</Page>
		);
	}
}
