import React from 'react';
import {Input} from "../../ui-kit/components/input/input.component";
import {Hr} from "../../ui-kit/components/hr/hr.component";
import {PaymentMethod} from "../../payment-method/payment-method.component";
import {Selectbox} from "../../ui-kit/components/selectbox/selectbox.component";
import { Col, Form, FormGroup, Label } from 'reactstrap';
import {Step} from "../../ui-kit/components/step/step.component";

import {MenuItem} from "@devexperts/react-kit/dist/components/Menu/Menu";
import {Badge} from "../../ui-kit/components/badge/badge.component";
import css from './theme/step5.component.module.styl'

export class Step5 extends React.Component {
	render() {
		return (
			<div className={css.container} >
				<div className={css.steps}>
					<Step isCompleted={true}>
						Ввод адреса
					</Step>
					<Step isCompleted={true}>
						Выбор помещения
					</Step>
					<Step isCurrent={true}>
						<strong>Оплата</strong>
					</Step>
					<Step isDisabled={true}>
						Мастер подготовки реестра
					</Step>
				</div>

				<Hr />

				<Form>
					<FormGroup row className="align-items-center">
						<Label for="exampleEmail" sm={1}>Плательщик</Label>
						<Col sm={10}>
							<Selectbox
								placeholder="Controlled by left"
								value={'superman'}
								theme={{}}
								onValueChange={() => console.log('t')}>
								<MenuItem value="superman">ООО «ЖКС1 Приморского района»</MenuItem>
								<MenuItem value="batman">Batman</MenuItem>
								<MenuItem value="flash">Flash</MenuItem>
							</Selectbox>
							<Badge>
								1000 помещений
							</Badge>
						</Col>
					</FormGroup>
					<FormGroup row className="align-items-center">
						<Label for="exampleEmail" sm={1}>Тариф</Label>
						<Col sm={10}>
							<Selectbox
								placeholder="Controlled by left"
								value={'superman'}
								theme={{}}
								onValueChange={() => console.log('t')}>
								<MenuItem value="superman">1000 помещений за 15 000 руб</MenuItem>
								<MenuItem value="batman">Batman</MenuItem>
								<MenuItem value="flash">Flash</MenuItem>
							</Selectbox>
							<Badge>
								1000 помещений
							</Badge>
						</Col>
					</FormGroup>
					<FormGroup row className="align-items-center">
						<Label for="exampleEmail" sm={1}>Тариф</Label>
						<Col sm={10}>
							<Input/>
						</Col>
					</FormGroup>
				</Form>





				<div className={css.payments}>
					<PaymentMethod title="Перевод на банковскую карту СберБанк «Онлайн»" image={i1Logo}
								   isChecked={true}/>
					<PaymentMethod title="Банковая карта (Yandex)" image={i2Logo}>
						Visa, MasterCard или Мир
					</PaymentMethod>
					<PaymentMethod title="Перевод на расчетный счет" image={i3Logo}>
						Безналичный платёж через банк
					</PaymentMethod>
				</div>

			</div>
		);
	}
}
