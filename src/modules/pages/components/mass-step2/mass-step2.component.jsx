import React from 'react';
import {Page} from "../../../ui-kit/components/page/page.component";
import {Col, Container, Row} from 'reactstrap';
import {ObjectsTable} from "../../../objects/objects-table/objects-table.component";
import {TableFooter} from "../../../ui-kit/components/table-footer/table-footer.component";

export class MassStep2 extends React.Component {
	render() {
		return (
			<Page withSteps={true} step={1} title="Мастер массовой подготовки реестра">
				<Container fluid>
					<Row>
						<Col xs={12}>
							<ObjectsTable />
							<TableFooter/>
						</Col>
					</Row>
				</Container>
			</Page>
		);
	}
}
