import React, {Fragment, Component} from 'react';
import cn from 'classnames';
import {Card} from "../../../ui-kit/components/card/card.component";
import {Button} from "../../../ui-kit/components/button/button.component";

import css from './theme/prepaid-statements.component.module.styl'
import cardTheme from './theme/card.child.module.styl';
import {Checkbox} from "../../../ui-kit/components/dx-checkbox/checkbox.component";

export class PrepaidStatement extends Component {

	render() {
		const {countSelected = 999, countAvailable = 800} = this.props;

		const isAvailableWriteOf = countAvailable > countSelected;

		const header = (
			<Fragment>
				ВЫБРАНО
				<div className={css.selected}>
					{countSelected} помещений
				</div>
			</Fragment>
		);

		const footer = (
			<div className={`d-flex a ${isAvailableWriteOf ? 'justify-content-center' : 'justify-content-around'}`}>
				<Button isPrimary={isAvailableWriteOf}>Списать</Button>
				{!isAvailableWriteOf && <Button isPrimary={true}>Доплатить</Button>}
			</div>
		);

		return (
			<Card theme={cardTheme} header={header} footer={footer}>
				<div className={css.body}>
					<div className={css.countAvailable} style={{
						color: !isAvailableWriteOf ?  '#ffbc00': '#27b031'
					}}>
						{countAvailable}
					</div>
					<div className={css.label}>Предоплаченных выписок</div>
					{!isAvailableWriteOf && (
						<div className={css.hint}>Не хватает выписок</div>
					)}
					<Checkbox value={true}>
						Использовать предоплаченные выписки
					</Checkbox>
				</div>
			</Card>
		);
	}
}
