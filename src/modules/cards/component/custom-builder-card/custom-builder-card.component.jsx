import React, {Fragment, Component} from 'react';
import {Card} from "../../../ui-kit/components/card/card.component";
import {Button} from "../../../ui-kit/components/button/button.component";
import {Input} from "../../../ui-kit/components/input/input.component";

import css from './theme/custom-builder-card.component.module.styl';
import chardTheme from './theme/card.child.module.styl';
import inputTheme from './theme/input.child.module.styl';

export class CustomBuilderCard extends Component {

	render() {
		const {countSelected = 999, value = true} = this.props;

		const header = this.props.header || (
			<Fragment>
				<span>Выбрано</span>
				<span>{countSelected} помещений</span>
			</Fragment>
		);
		return (
			<Card theme={chardTheme} footer={<Button isPrimary={true}>Списать</Button>} header={header}>
				<form className={css.container}>
					<div className={css.row}>
						<Input theme={inputTheme} value={250} />
						<div className={css.label}>Помещений</div>
						<div className={css.price}>
							30р
						</div>
					</div>
					<div className={css.row}>
						<Input theme={inputTheme} value={250} />
						<div className={css.label}>Собраний</div>
						<div className={css.price}>
							30р
						</div>
					</div>
					<div className={css.row}>
						<Input theme={inputTheme} value={3} />
						<div className={css.label}>Собраний</div>
						<div className={css.price}>
							3000 р
						</div>
					</div>
					{/*<div className={css.summary}>
						<div className={css.label}>Итого:</div>
						7 540 руб
					</div>*/}
				</form>
			</Card>
		);
	}
}
