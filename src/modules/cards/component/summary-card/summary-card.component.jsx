import React, {Fragment, Component} from 'react';
import cn from 'classnames';
import {Card} from "../../../ui-kit/components/card/card.component";
import {Button} from "../../../ui-kit/components/button/button.component";

import css from './theme/summary-card.component.module.styl'
import cardTheme from './theme/card.child.module.styl';

export class SummaryCard extends Component {

	render() {
		const {children, unloaded, total} = this.props;

		const header = (
			<div className={css.rows}>
				<div className={css.row}>
					<div className={css.label}>
						выгружено
					</div>
					<div className={css.counter}>
						<div className={css.unloaded}>
							{unloaded} помещений
						</div>
						<div className={css.total}>
							из {total} помещений
						</div>
					</div>
				</div>
				<div className={css.row}>
					<a href="#">Заказать оставшийся реест</a>
				</div>
			</div>
		);
		const footer = (
			<div className={css.footerInner}>
				<div className={css.footerHintText}>
					Подписаться на обновления
					данных об изменении прав
					на помещения во всех домах
				</div>
				<Button isExtra={true}>Подписаться</Button>
			</div>

		);

		return (
			<Card theme={cardTheme} header={header} footer={footer}>
				{children}
			</Card>
		);
	}
}
