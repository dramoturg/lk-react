import React from 'react';
import css from './theme/event-pack.component.module.styl'

export class EventPack extends React.Component {
	render() {
		const {price, discount} = this.props;

		return (
			<div className={css.container} >
				<div className={css.header}>
					При покупке единоразового
				</div>
				<div className={css.body}>
					от <span className={css.priceValue}>{price}</span> руб
				</div>
				<div className={css.footer}>
					скидка
					{discount && <div className={css.discount}>-{discount}%</div> }
				</div>
			</div>
		);
	}
}
