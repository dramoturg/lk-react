import React, {Fragment, Component} from 'react';
import {Card} from "../../../ui-kit/components/card/card.component";
import {Button} from "../../../ui-kit/components/button/button.component";
import {Checkbox} from "../../../ui-kit/components/dx-checkbox/checkbox.component";
import css from './theme/selected-tariff-card.component.module.styl';

export class SelectedTariffCard extends Component {

	render() {
		const {countSelected = 999, value = true} = this.props;

		const header = (
			<Fragment>
				<span>Выбрано</span>
				<span>{countSelected} помещений</span>
			</Fragment>
		);
		return (
			<Card theme={css} footer={<Button isPrimary={true}>Списать</Button>} header={header}>
				<div className={css.count}> 1000</div>
				<div className={css.countLabel}>Предоплаченных выписок</div>
				<div className={css.formControl}>
					<div className={css.checkbox}>
						<Checkbox value={value}/>
					</div>
					Использовать предоплаченные выгрузки
				</div>
			</Card>
		);
	}
}
