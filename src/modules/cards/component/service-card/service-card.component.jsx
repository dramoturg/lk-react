import React, {Fragment, Component} from 'react';
import cn from 'classnames';
import {Card} from "../../../ui-kit/components/card/card.component";
import {Button} from "../../../ui-kit/components/button/button.component";

import css from './theme/service-card.component.module.styl'
import buttonTheme from './theme/button.child.module.styl';

export const ServiceCardType = {
	Consult: 'consult',
	Zakaz: 'zakaz',
	Self: 'self'
};

export class ServiceCard extends Component {

	render() {
		const {type, children} = this.props;
		const theme = {
			...css,
			container: cn(css.container, {
				[css.container_consult]: type === ServiceCardType.Consult,
				[css.container_zakaz]: type === ServiceCard.Zakaz,
				[css.container_self]: type === ServiceCard.Self,
			})
		};
		return (
			<Card theme={theme} footer={<Button theme={buttonTheme} isPrimary={true}>Заказать</Button>}>
				{children}
			</Card>
		);
	}
}
