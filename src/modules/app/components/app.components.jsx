import React from 'react';
import {
	HashRouter as Router,
	Route
} from 'react-router-dom';
import {Layout} from '../../layout/components/layout.component';
import {withTheme} from "@devexperts/react-kit/dist/utils/withTheme";
import {Theme} from "../../ui-kit/components/theme/theme.component";
import {Step4Taraskin} from "../../pages/components/step4taraskin/step4taraskin.component";
import {Step4} from "../../pages/components/step4/step4";
import {Step1Taraskin} from "../../pages/components/step1taraskin/step1taraskin.component";
import {Step1} from "../../pages/components/step1/step1.components";
import {Step2} from "../../pages/components/step2/step2.component";
import {Step2Taraskin} from "../../pages/components/step2taraskin/step2taraskin.component";
import {Step5Taraskin} from "../../pages/components/step5taraskin/step5taraskin.component";
import {Step6} from "../../pages/components/step6/step6.component";


import css from './theme/app.component.styl';
import {Step3Taraskin} from "../../pages/components/step3taraskin/step3taraskin.component";
import {Step6Taraskin} from "../../pages/components/step6taraskin/step6taraskin.component";
import {Step3} from "../../pages/components/step3/step3.components";
import {MassStep1} from "../../pages/components/mass-step1/mass-step1.component";
import {MassStep2} from "../../pages/components/mass-step2/mass-step2.component";
import {MassStep3} from "../../pages/components/mass-step3/mass-step3.component";
import {MassStep4} from "../../pages/components/mass-step4/mass-step4.component";

class RawApp extends React.Component {
	render() {
		return (
			<Router>
				<Layout>
					<Route exact path="/" component={MassStep1}/>

					<Route exact path="/taraskin/1" component={Step1Taraskin}/>
					<Route exact path="/taraskin/2" component={Step2Taraskin}/>
					<Route exact path="/taraskin/3" component={Step3Taraskin}/>
					<Route exact path="/taraskin/4" component={Step4Taraskin}/>
					<Route exact path="/taraskin/5" component={Step5Taraskin}/>
					<Route exact path="/taraskin/6" component={Step6Taraskin}/>


					<Route exact path="/mass/step1" component={MassStep1}/>
					<Route exact path="/mass/step2" component={MassStep2}/>
					<Route exact path="/mass/step3" component={MassStep3}/>
					<Route exact path="/mass/step4" component={MassStep4}/>

					<Route exact path="/step1" component={Step1}/>
					<Route exact path="/step2" component={Step2}/>
					<Route exact path="/step3" component={Step3}/>
					<Route exact path="/step4" component={Step4}/>
				</Layout>
			</Router>
		);
	}
}

const ThemedApp = withTheme(Symbol(), css)(RawApp);
export const App  = props => {
	return (
		<Theme>
			<ThemedApp {...props} />
		</Theme>
	);
};
