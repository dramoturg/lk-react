const path = require("path");
const rewireStyl = require("react-app-rewire-stylus-modules");
const rewireSass = require('react-app-rewire-scss');

const ROOT = path.join(process.cwd());
const SRC_PATH = path.resolve(ROOT, 'src');


module.exports = function override(config, env) {
	const configWithStylus = rewireStyl(config, env, {});
	config = rewireSass(configWithStylus, env);
	const resolve = config.resolve;
	config.resolve = {
		...resolve,
		alias: {
			...resolve.alias,
			config: path.resolve(SRC_PATH, 'config/styles.config.styl'),
		}
	};
	return config;
}
